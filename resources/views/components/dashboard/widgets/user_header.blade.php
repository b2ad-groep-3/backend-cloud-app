<div class="md:flex md:items-center md:justify-between md:space-x-5 bg-white p-5 border-b border-gray-200 shadow-sm">
    <div class="flex items-start space-x-5">
        <div class="flex-shrink-0">
            <div class="relative">
                <img class="h-16 w-16 rounded-full" src="{{ asset('img/default_user.jpg') }}" alt="">
                <span class="absolute inset-0 shadow-inner rounded-full" aria-hidden="true"></span>
            </div>
        </div>
        <div class="pt-1.5">
            <h1 class="text-2xl font-bold text-gray-900">{{ auth()->user()->name }}</h1>
            <p class="text-sm font-medium text-gray-500">{{ __('zuydpresence.account_created') }} {{ auth()->user()->created_at->diffForHumans() }}</p>
        </div>
    </div>
    <div class="mt-6 flex flex-col-reverse justify-stretch space-y-4 space-y-reverse sm:flex-row-reverse sm:justify-end sm:space-x-reverse sm:space-y-0 sm:space-x-3 md:mt-0 md:flex-row md:space-x-3">
        <a href="{{ route('teachers.create') }}">
            <button type="button" class="inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500">
                {{ __('zuydpresence.create_new_teacher') }}
            </button>
        </a>
    </div>
</div>