<div>
    <h3 class="text-lg leading-6 font-medium text-gray-900">
        {{ __('zuydpresence.all_time_stats') }}
    </h3>
    <dl class="mt-5 grid grid-cols-1 gap-5 sm:grid-cols-3">
        <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
            <dt class="text-sm font-medium text-gray-500 truncate">
                {{ __('zuydpresence.teacher_count') }}
            </dt>
            <dd class="mt-1 text-3xl font-semibold text-gray-900">
                {{ $statistics['teacher_count'] }}
            </dd>
        </div>

        <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
            <dt class="text-sm font-medium text-gray-500 truncate">
                {{ __('zuydpresence.teacher_present_count') }}
            </dt>
            <dd class="mt-1 text-3xl font-semibold text-gray-900">
                {{ $statistics['present_count'] }}
            </dd>
        </div>

        <div class="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6">
            <dt class="text-sm font-medium text-gray-500 truncate">
                {{ __('zuydpresence.api_count') }}
            </dt>
            <dd class="mt-1 text-3xl font-semibold text-gray-900">
                {{ $statistics['active_api_count'] }}
            </dd>
        </div>
    </dl>
</div>