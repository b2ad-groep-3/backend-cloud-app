<div class="bg-white shadow overflow-hidden sm:rounded-lg">
    <div class="px-4 py-5 sm:px-6">
        <h3 class="text-lg leading-6 font-medium text-gray-900">
            {{ __('zuydpresence.edit') }} {{ __('zuydpresence.branch') }} {{ $branch->name }}
        </h3>
        <p class="mt-1 max-w-2xl text-sm text-gray-500">
            {{ __('zuydpresence.branch') }} {{ __('zuydpresence.details') }}.
        </p>
    </div>
    <form method="post" action="{{ route('branches.update', ['branch' => $branch]) }}">

        @csrf

        <div class="border-t border-b border-gray-200 px-4 py-5 sm:p-0">
            <dl class="sm:divide-y sm:divide-gray-200">
                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.name') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <input value="{{ $branch->name }}" type="text" name="name" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.name') }}">
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.street') }} {{ __('zuydpresence.and') }} {{ __('zuydpresence.house_number') }}
                    </dt> 
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">

                        <div class="grid grid-cols-6 gap-6">

                            <div class="grid col-span-4 xs:col-span-4">
                                <input value="{{ $branch->street }}" type="text" name="street" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.street') }}">
                            </div>

                            <div class="grid col-span-1 xs:col-span-2">
                                <input value="{{ $branch->house_number }}" type="text" name="house_number" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.house_number') }}">
                            </div>

                        </div>
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.city') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <input value="{{ $branch->city }}" type="text" name="city" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.city') }}">
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.postal_code') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <input value="{{ $branch->postal_code }}" type="text" name="postal_code" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.postal_code') }}">
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.state_province_county') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <input value="{{ $branch->province_state_county }}" type="text" name="province_state_county" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.state_province_county') }}">
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.country') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <select name="country_code" class="flex-1 min-w-0 block w-full px-3 py-2 rounded-md focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300">
                            
                            @foreach ($countries as $country)
                                <option value="{{ $country->code }}" {{ $country->code == $branch->country->code ? 'selected' : '' }}>{{ $country->name }}</option>
                            @endforeach

                        </select>
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.departments') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        
                        @if(count($branch->departments) > 0)

                            @foreach($branch->departments as $department)
                                <div class="mt-1 flex rounded-md pb-5 department-input" id="department-input-set">
                                    <input value="{{ $department->name }}" id="department" type="text" name="departments[]" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full rounded-l-md sm:text-sm border-r-0 border-l-md border-gray-300" placeholder="{{ __('zuydpresence.name') }}">
                                    <span class="inline-flex items-center px-3 rounded-r-md border border-l-md border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                        <button type="button" id="remove-department" onClick="remove_closest_input(event)">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                            </svg>
                                        </button>
                                    </span>
                                </div>
                            @endforeach

                        @else

                            <div class="mt-1 flex rounded-md pb-5 department-input" id="department-input-set">
                                <input id="department" type="text" name="departments[]" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full rounded-l-md sm:text-sm border-r-0 border-l-md border-gray-300" placeholder="{{ __('zuydpresence.name') }}">
                                <span class="inline-flex items-center px-3 rounded-r-md border border-l-md border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                    <button type="button" id="remove-department" onClick="remove_closest_input(event)">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    </button>
                                </span>
                            </div>

                        @endif

                        <div class="text-right" id="input-insertion-point">
                            <button id="add-department" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                            </button>
                        </div>
                    </dd>
                </div>
            </dl>
        </div>
        <div class="p-5 text-right">
            <input value="{{ __('zuydpresence.save') }}" type="submit" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
        </div>
    <form>
</div>

<script>
    add_department_button = document.getElementById('add-department')
    prepend_point = document.getElementById('input-insertion-point')

    add_department_button.addEventListener('click', function () {
        department_input_initial = document.getElementById('department-input-set')
        department_input = department_input_initial.cloneNode(true)
        prepend_point.prepend(department_input)
    });

    function remove_closest_input(event)
    {
        // dont remove last input
        if(document.querySelectorAll('.department-input').length == 1)
        {
            return
        }

        clicked_element = event.target
        parent_div = clicked_element.closest('.department-input')
        parent_div.remove()
    }
</script>