<div class="bg-white shadow overflow-hidden sm:rounded-lg">
    <div class="px-4 py-5 sm:px-6">
        <h3 class="text-lg leading-6 font-medium text-gray-900">
            {{ __('zuydpresence.teacher') }} {{ $teacher->first_name }} {{ $teacher->middle_name }} {{ $teacher->last_name }}
        </h3>
        <p class="mt-1 max-w-2xl text-sm text-gray-500">
            {{ __('zuydpresence.teacher') }} {{ __('zuydpresence.details') }}.
        </p>
    </div>
    <div class="border-t border-b border-gray-200 px-4 py-5 sm:p-0">
        <dl class="sm:divide-y sm:divide-gray-200">

            <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">
                    {{ __('zuydpresence.name') }} ID
                </dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {{ $teacher->first_name }} {{ $teacher->middle_name }} {{ $teacher->last_name }}
                </dd>
            </div>

            <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">
                    {{ __('zuydpresence.email') }}
                </dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                    {{ $teacher->email }}
                </dd>
            </div>

            <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">
                    {{ __('zuydpresence.branch') }}
                </dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2 truncate">
                    {{ $teacher->branch->name }}
                </dd>
            </div>

            <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">
                    {{ __('zuydpresence.department') }}
                </dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2 truncate">
                    @if(isset($teacher->department))
                        {{ $teacher->department->name }}
                    @else
                        {{ __('zuydpresence.teacher_has_no_department') }}
                    @endif
                </dd>
            </div>

            <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">
                    {{ __('zuydpresence.photos') }}
                </dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2 truncate">
                    @if(count($teacher->photos) > 0)
                        
                        <ul class="mt-2 w-full text-sm font-medium text-gray-900 bg-white rounded-lg border border-gray-200 dark:bg-gray-700 dark:border-gray-600 dark:text-white">

                            @foreach($teacher->photos as $photo)
                                <li class="text-gray-500 py-2 px-4 w-full border-b border-gray-200 dark:border-gray-600 align-middle">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13" />
                                    </svg>

                                    <span class="inline align-middle text-gray-500">
                                        {{ $photo->name }}
                                        <span class="text-right float-right text-indigo-500">
                                            <a target="_blank" href="{{ url('/uploads/' . $photo->name) }}">{{ __('zuydpresence.view') }}</a>
                                        </span>
                                    </span>
                                </li>
                            @endforeach
                                          
                        </ul>
                    @else
                        {{ __('zuydpresence.no_photos') }}
                    @endif
                </dd>
            </div>
        </dl>
    </div>
</div>
