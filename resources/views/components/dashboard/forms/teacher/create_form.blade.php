<div class="bg-white shadow overflow-hidden sm:rounded-lg">
    <div class="px-4 py-5 sm:px-6">
        <h3 class="text-lg leading-6 font-medium text-gray-900">
            {{ __('zuydpresence.create_teacher') }}
        </h3>
        <p class="mt-1 max-w-2xl text-sm text-gray-500">
            {{ __('zuydpresence.teacher') }} {{ __('zuydpresence.details') }}.
        </p>
    </div>
    <form method="post" action="{{ route('teachers.store') }}" enctype="multipart/form-data">

        @csrf

        <div class="border-t border-b border-gray-200 px-4 py-5 sm:p-0">
            <dl class="sm:divide-y sm:divide-gray-200">

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.name') }}
                    </dt> 
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">

                        <div class="grid grid-cols-6 gap-6">

                            <div class="grid col-span-4 xs:col-span-2">
                                <input type="text" name="first_name" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.first_name') }}">
                            </div>

                            <div class="grid col-span-1 xs:col-span-2">
                                <input type="text" name="middle_name" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.middle_name') }}">
                            </div>

                            <div class="grid col-span-1 xs:col-span-2">
                                <input type="text" name="last_name" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.last_name') }}">
                            </div>

                        </div>
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.email') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <input type="text" name="email" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.email') }}">
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.branch') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <select name="branch_id" class="flex-1 min-w-0 block w-full px-3 py-2 rounded-md focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300">
                            
                            @foreach ($branches as $branch)
                                <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                            @endforeach

                        </select>
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.photos') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">

                        <div class="w-full h-48 border-gray-300 border-2 border-dashed rounded-lg flex justify-center mb-2">

                            <div class="text-center m-auto">
                                <a class="text-indigo-800 cursor-pointer" onClick="document.getElementById('fileInput').click()">Browse Files</a>
                            </div>

                        </div>

                        <span id="uploadedFileList">No files uploaded.</span>

                        <input onChange="updateFileList()" class="hidden" id="fileInput" type="file" name="photos[]" multiple accept="image/png, image/jpeg">

                    </dd>
                </div>
            </dl>
        </div>
        <div class="p-5 text-right">
            <input value="{{ __('zuydpresence.save') }}" type="submit" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
        </div>
    <form>
</div>

<script>
    fileInput = document.getElementById("fileInput");   
    uploadList = document.getElementById("uploadedFileList")

    function updateFileList()
    {
        file_names = []

        for(const file of fileInput.files)
        {
            file_names.push(file.name)
        }

        console.log(file_names.length)

        if(file_names.length > 0)
        {
            file_names_string = file_names.join(", ")
            uploadList.textContent = file_names_string

            console.log(file_names_string)
        }
        else
        {
            uploadList.textContent = "No files uploaded."
        }
    }
</script>