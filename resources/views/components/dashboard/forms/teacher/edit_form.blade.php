<div class="bg-white shadow overflow-hidden sm:rounded-lg">
    <div class="px-4 py-5 sm:px-6">
        <h3 class="text-lg leading-6 font-medium text-gray-900">
            {{ __('zuydpresence.edit') }} {{ __('zuydpresence.teacher') }} {{ $teacher->first_name }} {{ $teacher->middle_name }} {{ $teacher->last_name }}
        </h3>
        <p class="mt-1 max-w-2xl text-sm text-gray-500">
            {{ __('zuydpresence.teacher') }} {{ __('zuydpresence.details') }}.
        </p>
    </div>
    <form method="post" action="{{ route('teachers.update', ['teacher' => $teacher]) }}" enctype="multipart/form-data">

        @csrf

        <div class="border-t border-b border-gray-200 px-4 py-5 sm:p-0">
            <dl class="sm:divide-y sm:divide-gray-200">

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.name') }}
                    </dt> 
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">

                        <div class="grid grid-cols-6 gap-6">

                            <div class="grid col-span-4 xs:col-span-2">
                                <input value="{{ $teacher->first_name }}" type="text" name="first_name" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.first_name') }}">
                            </div>

                            <div class="grid col-span-1 xs:col-span-2">
                                <input value="{{ $teacher->middle_name }}" type="text" name="middle_name" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.middle_name') }}">
                            </div>

                            <div class="grid col-span-1 xs:col-span-2">
                                <input value="{{ $teacher->last_name }}" type="text" name="last_name" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.last_name') }}">
                            </div>

                        </div>
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.email') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <input value="{{ $teacher->email }}" type="text" name="email" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.email') }}">
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.branch') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <select name="branch_id" class="flex-1 min-w-0 block w-full px-3 py-2 rounded-md focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300">
                            
                            @foreach ($branches as $branch)
                                <option value="{{ $branch->id }}" {{ $branch->id == $teacher->branch_id ? 'selected' : '' }}>{{ $branch->name }}</option>
                            @endforeach

                        </select>
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.department') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <select name="department_id" class="flex-1 min-w-0 block w-full px-3 py-2 rounded-md focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300">
                            
                            <option value="" {{ !isset($teacher->department_id) ? 'selected' : '' }}>{{ __('zuydpresence.teacher_has_no_department') }}</option>

                            @foreach ($departments as $department)
                                <option value="{{ $department->id }}" {{ $department->id == $teacher->department_id ? 'selected' : '' }}>{{ $department->name }}</option>
                            @endforeach

                        </select>
                    </dd>
                </div>

                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt class="text-sm font-medium text-gray-500">
                        {{ __('zuydpresence.photos') }}
                    </dt>
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">

                        @if(isset($teacher->photos))
                            <div id="existingFileList">

                                @foreach($teacher->photos as $photo)
                                    <div class="mt-1 flex rounded-md pb-5 photo-input" id="photo-input">

                                        <a target="_blank" href="{{ url('/uploads/' . $photo->name) }}" class="cursor-pointer inline-flex items-center px-4 mr-3 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                                            </svg>
                                        </a>

                                        <input name="existing_photos[]" value="{{ $photo->name }}" readonly id="photo" type="text" class="bg-gray-50 shadow-sm block w-full rounded-l-md sm:text-sm border-r-0 border-l-md border-gray-300 focus:border-gray-300 focus:outline-none" placeholder="{{ __('zuydpresence.name') }}">
                                        
                                        <span class="inline-flex items-center px-3 rounded-r-md border border-l-md border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                            <button type="button" id="remove-department" onClick="remove_closest_input(event)">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                            </button>
                                        </span>
                                    </div>
                                @endforeach

                            </div>
                        @endif

                        <div class="w-full h-48 border-gray-300 border-2 border-dashed rounded-lg flex justify-center mb-2">

                            <div class="text-center m-auto">
                                <a class="text-indigo-800 cursor-pointer" onClick="document.getElementById('fileInput').click()">Browse Files</a>
                            </div>

                        </div>

                        <span id="uploadedFileList">No new files uploaded.</span>

                        <input onChange="updateFileList()" class="hidden" id="fileInput" type="file" name="photos[]" multiple accept="image/png, image/jpeg">
                    </dd>
                </div>
            </dl>
        </div>
        <div class="p-5 text-right">
            <input value="{{ __('zuydpresence.save') }}" type="submit" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
        </div>
    <form>
</div>

<script>
    function remove_closest_input(event)
    {
        clicked_element = event.target
        parent_div = clicked_element.closest('#photo-input')
        parent_div.remove()
    }
</script>

<script>
    fileInput = document.getElementById("fileInput");   
    uploadList = document.getElementById("uploadedFileList")

    function updateFileList()
    {
        file_names = []

        for(const file of fileInput.files)
        {
            file_names.push(file.name)
        }

        console.log(file_names.length)

        if(file_names.length > 0)
        {
            file_names_string = file_names.join(", ")
            uploadList.textContent = file_names_string

            console.log(file_names_string)
        }
        else
        {
            uploadList.textContent = "No new files uploaded."
        }
    }
</script>