@if($branches->count() > 0)
    <div>

        {{-- Header. --}}
        <h3 class="text-lg leading-6 font-medium text-gray-900 mb-5">
            {{ __('zuydpresence.branches') }}
        </h3>

        {{-- Table. --}}
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table class="min-w-full divide-y divide-gray-200">
                            <thead class="bg-gray-50">
                                <tr>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        #
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('zuydpresence.name') }}
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('zuydpresence.address') }}
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('zuydpresence.country') }}
                                    </th>
                                    <th scope="col" class="relative px-6 py-3">
                                        <a href="{{ route('branches.create') }}" class="text-indigo-600 float-right">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                            </svg>
                                        </a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">

                                @foreach($branches as $branch)
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap truncate">
                                            {{ $branch->id }}
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap truncate">
                                            {{ $branch->name }}
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{ $branch->street }} {{ $branch->house_number }}</div>
                                            <div class="text-sm text-gray-500">{{ $branch->postal_code }} {{ $branch->city }}</div>
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap">
                                            {{ $branch->country->name }}
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                            <form action="{{ route('branches.delete', ['id' => $branch->id]) }}" method="POST" class="inline-block">

                                                @csrf
                                                @method('delete')

                                                <button type="submit" class="text-red-500 inline-block align-middle">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                    </svg>
                                                </button>
                                            </form>
                                            <a href="{{ route('branches.edit', ['branch' => $branch]) }}" class="text-indigo-500 inline-block align-middle ml-5">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                                </svg>
                                            </a>
                                            <a href="{{ route('branches.view', ['branch' => $branch]) }}" class="text-indigo-500 inline-block align-middle ml-5">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>

                    </div>

                    <div class="mt-4">
                        {{ $branches->links() }}
                    </div>

                </div>
            </div>
        </div>

    </div>
@else
    <div class="min-h-full min-w-full flex items-center justify-center">
        <div class="empty-state text-center">
            <div class="empty-state-icon text-gray-400">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4" />
                </svg>
            </div>
            <div class="empty-state-title text-gray-900">
                No branches
            </div>
            <div class="empty-state-text text-gray-600">
                Get started by creating one.
            </div>
            <div class="empty-state-call-to-action">
                <a href="{{ route('branches.create') }}" class="text-white bg-indigo-700 py-2 px-4 rounded inline-flex items-center align-middle mt-3">
                    <span>Create branch</span>
                </a>
            </div>
        <div>
    </div>
@endif