<div>

    {{-- Header. --}}
    <h3 class="text-lg leading-6 font-medium text-gray-900 mb-5">
        {{ __('nav.presence') }}
    </h3>

    {{-- Table. --}}
    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                            <tr>
                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('zuydpresence.name') }}
                                </th>
                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('zuydpresence.email') }}
                                </th>
                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('zuydpresence.branch') }}
                                </th>
                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('zuydpresence.status') }}
                                </th>
                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('zuydpresence.last_updated') }}
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">

                            @foreach($statuses as $status)
                                <tr>
                                    <td class="px-6 py-4 whitespace-nowrap truncate">
                                        {{ $status->teacher->first_name }} {{ $status->teacher->middle_name }} {{ $status->teacher->last_name }}
                                    </td>

                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{ $status->teacher->email }}
                                    </td>

                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{ $status->branch->name }}
                                    </td>

                                    <td class="px-6 py-4 whitespace-nowrap">
                                        @if($status->present)
                                            <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800">
                                                {{ __('zuydpresence.present') }}
                                            </span>
                                        @else
                                            <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-red-100 text-red-800">
                                                {{ __('zuydpresence.not_present') }}
                                            </span>
                                        @endif
                                    </td>

                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{ $status->updated_at->diffForHumans() }}
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>

                <div class="mt-4">
                    {{ $statuses->links() }}
                </div>

            </div>
        </div>
    </div>

</div>