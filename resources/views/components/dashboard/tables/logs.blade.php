@if($logs->count() > 0)
    <div>

        {{-- Header. --}}
        <h3 class="text-lg leading-6 font-medium text-gray-900 mb-5">
            {{ __('zuydpresence.logs') }}
        </h3>

        {{-- Toolbar. --}}
        <div class="toolbar mb-4">
            <form method="post" action="{{ route('logs.filter') }}" class="inline">

                @csrf

                Filter
                <select name="filter" class="inline-block rounded-md focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300">
                    <option {{ Request::is('app/logs') ? 'selected' : '' }}  value="all">All</option>
                    <option {{ Request::is('app/logs/system') ? 'selected' : '' }} value="system">System</option>
                    <option {{ Request::is('app/logs/api') ? 'selected' : '' }} value="api">Api</option>
                    <option {{ Request::is('app/logs/other') ? 'selected' : '' }} value="other">Other</option>
                </select>

                <button type="submit" class="px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Apply
                </button>
            </form>
            <form method="post" action="{{ route('logs.clear') }}" class="inline-block float-right">

                @csrf

                <button type="submit" class="px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">
                    Clear logs
                </button>
            </form>
        </div>

        {{-- Table. --}}
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table class="min-w-full divide-y divide-gray-200">
                            <thead class="bg-gray-50">
                                <tr>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('zuydpresence.time') }}
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('zuydpresence.entry') }}
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('zuydpresence.type') }}
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('zuydpresence.group') }}
                                    </th>
                                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">

                                @php $i = 1 @endphp

                                @foreach($logs as $log)
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap truncate align-middle">
                                            @if($log->severity == 'error')
                                                <span class="align-middle inline-flex items-center rounded-full text-xs font-medium text-red-800">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                </span>
                                            @elseif($log->severity == 'info')
                                                <span class="align-middle inline-flex items-center rounded-full text-xs font-medium text-indigo-800">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                </span>
                                            @elseif($log->severity == 'warning')
                                                <span class="align-middle inline-flex items-center rounded-full text-xs font-medium text-yellow-800">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                                                    </svg>
                                                </span>
                                            @elseif($log->severity == 'success')
                                                <span class="align-middle inline-flex items-center rounded-full text-xs font-medium text-green-800">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                </span>
                                            @endif
                                            <span class="align-middle ml-3">
                                                {{ $log->created_at }}
                                            </span>
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap">
                                            {{ $log->entry }}
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap">
                                            @if($log->severity == 'error')
                                                <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-red-100 text-red-800">
                                                    Error
                                                </span>
                                            @elseif($log->severity == 'info')
                                                <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-indigo-100 text-indigo-800">
                                                    Info
                                                </span>
                                            @elseif($log->severity == 'warning')
                                                <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-yellow-100 text-yellow-800">
                                                    Warning
                                                </span>
                                            @elseif($log->severity == 'success')
                                                <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800">
                                                    Success
                                                </span>
                                            @endif
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <span class="capitalize inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-gray-100 text-gray-800">
                                                {{ $log->group }}
                                            </span>
                                        </td>

                                        <td class="text-right px-6 py-4 whitespace-nowrap">
                                            <a class="text-indigo-800 cursor-pointer" onclick="showDetails({{$i}})">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 13l-7 7-7-7m14-8l-7 7-7-7" />
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>

                                    <tr class="details hidden bg-gray-50" id="details{{ $i }}">
                                        <td colspan="5" class="px-6 py-4 whitespace-nowrap">

                                            @if(!array_filter([$log->ecode, $log->status_code, $log->api_key, $log->signature]))
                                                <span>
                                                    {{ __('zuydpresence.no_additional_request_data') }}.
                                                </span>
                                            @endif

                                            @if(array_filter([$log->ecode, $log->status_code, $log->api_key, $log->signature]))

                                                <span>
                                                    {{ __('zuydpresence.additional_request_data') }}.
                                                </span>

                                                <ul class="mt-2 w-full text-sm font-medium text-gray-900 bg-white rounded-lg border border-gray-200 dark:bg-gray-700 dark:border-gray-600 dark:text-white">
                                                    @if(isset($log->ecode))
                                                        <li class="text-gray-600 py-2 px-4 w-full border-b border-gray-200 dark:border-gray-600 align-middle">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                            </svg>

                                                            <span class="inline">
                                                                {{ $log->ecode }}
                                                            </span>
                                                        </li>
                                                    @endif

                                                    @if(isset($log->status_code))
                                                        <li class="text-gray-600 py-2 px-4 w-full border-b border-gray-200 dark:border-gray-600 align-middle inline-flex items-center">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.636 18.364a9 9 0 010-12.728m12.728 0a9 9 0 010 12.728m-9.9-2.829a5 5 0 010-7.07m7.072 0a5 5 0 010 7.07M13 12a1 1 0 11-2 0 1 1 0 012 0z" />
                                                            </svg>

                                                            <span class="inline">
                                                                {{ $log->status_code }} - {{ __('status_codes.' . $log->status_code . '.description') }}
                                                            </span>
                                                        </li>
                                                    @endif

                                                    @if(isset($log->api_key))
                                                        <li class="text-gray-600 py-2 px-4 border-b border-gray-200 dark:border-gray-600 align-middle">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 7a2 2 0 012 2m4 0a6 6 0 01-7.743 5.743L11 17H9v2H7v2H4a1 1 0 01-1-1v-2.586a1 1 0 01.293-.707l5.964-5.964A6 6 0 1121 9z" />
                                                            </svg>

                                                            <span class="inline">
                                                                {{ $log->api_key }}
                                                            </span>
                                                        </li>
                                                    @endif

                                                    @if(isset($log->signature))
                                                        <li class="text-gray-600 py-2 px-4 w-full rounded-b-lg border-gray-200 dark:border-gray-600 align-middle inline-flex items-center">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 11c0 3.517-1.009 6.799-2.753 9.571m-3.44-2.04l.054-.09A13.916 13.916 0 008 11a4 4 0 118 0c0 1.017-.07 2.019-.203 3m-2.118 6.844A21.88 21.88 0 0015.171 17m3.839 1.132c.645-2.266.99-4.659.99-7.132A8 8 0 008 4.07M3 15.364c.64-1.319 1-2.8 1-4.364 0-1.457.39-2.823 1.07-4" />
                                                            </svg>

                                                            <span class="inline">
                                                                {{ $log->signature }}
                                                            </span>
                                                        </li>
                                                    @endif
                                                </ul>
                                            @endif
                                        <td>
                                    <tr>

                                    @php $i++ @endphp
                                @endforeach

                            </tbody>
                        </table>

                    </div>

                    <div class="mt-4">
                        {{ $logs->links() }}
                    </div>

                </div>
            </div>
        </div>

    </div>

    <script>

        function showDetails(caller)
        {
            target = document.getElementById("details" + caller)

            console.log(target)

            if(target.classList.contains('hidden'))
            {
                target.classList.remove('hidden')
            }
            else
            {
                target.classList.add('hidden')
            }
        }

    </script>
@else
    <div class="min-h-full min-w-full flex items-center justify-center">
        <div class="empty-state text-center">
            <div class="empty-state-icon text-gray-400">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 13V6a2 2 0 00-2-2H6a2 2 0 00-2 2v7m16 0v5a2 2 0 01-2 2H6a2 2 0 01-2-2v-5m16 0h-2.586a1 1 0 00-.707.293l-2.414 2.414a1 1 0 01-.707.293h-3.172a1 1 0 01-.707-.293l-2.414-2.414A1 1 0 006.586 13H4" />
                </svg>
            </div>
            <div class="empty-state-title text-gray-900">
                No logs found
            </div>
            <div class="empty-state-text text-gray-600">
                You're all caught up!
            </div>
        <div>
    </div>
@endif