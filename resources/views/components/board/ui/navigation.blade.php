<nav class="bg-white border-b-2">
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex items-center justify-between h-16">
            <div class="flex items-center">
                <div class="flex-shrink-0">
                    <svg class="h-8 w-8" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 148 160" enable-background="new 0 0 148 160" xml:space="preserve" fill="#ed1b34">
						<path id="_x38_2d6a" d="M111.029,105.4c5.201,0,11.75-0.11,16.371,6.06c3.09,4.24,3.57,8.96,3.57,13.681
							c0,19.569-12.521,20.039-21.591,20.239h-5.2v-39.989L111.029,105.4z M87.029,91.03v68.03h26.801c7.109-0.101,26.1-0.48,32.37-21.48
							c0.86-2.9,1.729-7.24,1.729-13.12c0-9.53-2.699-19.069-9.729-25.43c-8.671-7.71-21.2-7.9-29.49-8H87.029z"
                        />
						<path id="_x38_2d6b" d="M0.82,91.03h19.56l14.65,23.319c0.779-1.78,1.716-3.486,2.8-5.1l11.65-18.22h19.38
							l-25.74,35.85v32.181H26.36v-31.701L0.82,91.02"
                        />
						<path id="_x38_2d6c" d="M145.141,0.32v37.96c-0.101,6.56-0.201,13.11-3.951,19.28
							c-7.33,11.75-22.459,12.33-25.35,12.33c-5.1,0-11.18-1.25-15.8-3.66c-13.101-6.75-13.101-18.7-13-27.95V0.32h17.14v42.4
							c0,3.47,0.1,6.36,1.26,8.48c2.11,3.56,6.74,4.62,10.98,4.62c11.561,0,11.561-7.9,11.67-13.2V0.32H145.141"
                        />
						<path id="_x38_2d6d" d="M9.06,0.32h54.92v13.3L26.9,54.95h36.9v13.4H5.88V55.63l37.68-42.02H9.06V0.32"/>
					</svg>
                </div>
                    
                <div class="hidden md:block">
                    <div class="ml-10 flex items-baseline space-x-4">
                        <a href="{{ route('presenceboard.global') }}" class="text-red-500 px-3 py-2 rounded-md text-sm font-medium" aria-current="page">{{ __('zuydpresence.global') }}</a>

                        <div class="relative flex-shrink-0">
                            <div class="align-middle flex flex-col items-center">
                                <button id="dropdown-open-button" onclick="show()" type="button" class="text-red-500 px-3 py-2 rounded-md text-sm font-medium flex items-center" aria-expanded="false" aria-haspopup="true">
                                    {{ __('zuydpresence.branch') }}
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 ml-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                                    </svg>
                                </button>
                            </div>
                            <div id="dropdown-content" class="invisible origin-top-left absolute left-0 mt-2 rounded-md w-64 shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none z-30" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">
                                @foreach($filters as $filter)
                                    <a href="{{ route('presenceboard.focus', ['id' => $filter->id]) }}" class="text-red-500 px-3 py-2 block">{{ $filter->name }}</a>
                                @endforeach
                            </div>
                        </div>
                        <div>
                            <div class="align-middle flex flex-col items-center">
                                <button id="dropdown-open-button" onclick="showlang()" type="button" class="text-red-500 px-3 py-2 rounded-md text-sm font-medium flex items-center" aria-expanded="false" aria-haspopup="true">
                                    {{ __('zuydpresence.language') }}
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 ml-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                                    </svg>
                                </button>
                            </div>
                            <div id="lang-dropdown" class="invisible origin-top-left absolute mt-2 rounded-md w-64 shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none z-30" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">
                                @foreach(config('zuydpresence.supported_locales') as $locale => $code)
                                    <button onclick="updateLocale('{{ $code }}')" type="button" class="text-red-500 px-3 py-2 block">{{ $locale }}</button>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <form action="{{ route('locale.save') }}" method="POST" id="updateLocaleForm">
                @csrf
                <input type="hidden" id="locale" name="locale" value="{{ app()->getLocale() }}">
            </form>

            {{-- Right aligned top nav content. --}}
            <div class="hidden md:block">
                <div class="ml-4 flex items-center md:ml-6 text-red-500 px-3">
                    <p class="font-medium">{{ date("d-m-Y") }} <span id="span" class="mr-6 text-base font-medium"></span></p>
                    <a href="{{ route('dashboard.home') }}" target="_blank" class="px-4 py-2 bg-red-500 text-white font-semibold rounded-lg text-sm">
                        {{ __('zuydpresence.management_application') }}
                    </a>
                </div>
            </div>

            <div class="-mr-2 flex md:hidden">
                <button type="button" onclick="showmenu()" class="bg-red-500 inline-flex items-center justify-center p-2 rounded-md text-white hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-white" aria-controls="mobile-menu" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>

                    <svg class="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    </svg>

                    <svg class="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <div class="hidden" id="mobile-menu">
        <hr>
        <div class="px-2 pt-2 pb-3 space-y-1 sm:px-3">
            <a href="{{ route('presenceboard.global') }}" class="{{ Request::is('presence-board') ? 'bg-red-500 text-white' : 'text-red-500' }} block px-3 py-2 rounded-md text-base font-medium hover:text-white hover:bg-red-500" aria-current="page">{{ __('zuydpresence.global_board') }}</a>
        </div>
        <hr>
        <div class="px-2 pt-2 pb-3 space-y-1 sm:px-3">
            @foreach($filters as $filter)
                <a href="{{ route('presenceboard.focus', ['id' => $filter->id]) }}" class="{{ Request::is('presence-board/' . $filter->id) ? 'bg-red-500 text-white' : 'text-red-500' }} px-3 py-2 rounded-md text-base font-medium block hover:text-white hover:bg-red-500">{{ $filter->name }}</a>
            @endforeach
        </div>
        <hr>
        <div class="px-2 pt-2 pb-3 space-y-1 sm:px-3">
            <a href="{{ route('dashboard.home') }}" target="_blank" class="block px-3 py-2 rounded-md text-base font-medium text-red-500 hover:text-white hover:bg-red-500" aria-current="page">{{ __('zuydpresence.management_application') }}</a>
        </div>
    </div>
</nav>

<header class="bg-gray-50 shadow">
    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <h1 class="text-3xl font-bold text-red-500">
            @yield('branch', __('zuydpresence.global_board'))
        </h1>
    </div>
</header>

<script>
    content = document.getElementById('dropdown-content')
    mobile_menu = document.getElementById('mobile-menu')
    lang_menu = document.getElementById('lang-dropdown')

    function show()
    {
        // Close other dropdown.
        if(lang_menu.classList.contains('visible'))
        {
            lang_menu.classList.remove('visible')
            lang_menu.classList.add('invisible')
        }

        if(content.classList.contains('visible'))
        {
            content.classList.remove('visible')
            content.classList.add('invisible')
        }
        else
        {
            content.classList.remove('invisible')
            content.classList.add('visible')
        }
    }

    function showmenu()
    {
        if(mobile_menu.classList.contains('hidden'))
        {
            mobile_menu.classList.remove('hidden')
        }
        else
        {
            mobile_menu.classList.add('hidden')
        }
    }

    function showlang()
    {
        // Close other dropdown.
        // Close other dropdown.
        if(content.classList.contains('visible'))
        {
            content.classList.remove('visible')
            content.classList.add('invisible')
        }

        if(lang_menu.classList.contains('visible'))
        {
            lang_menu.classList.remove('visible')
            lang_menu.classList.add('invisible')
        }
        else
        {
            lang_menu.classList.remove('invisible')
            lang_menu.classList.add('visible')
        }
    }

    function updateLocale(lang)
    {
        document.getElementById('locale').value = lang
        document.getElementById('updateLocaleForm').submit()
    }

    var span = document.getElementById('span');

    function time() {
    var d = new Date();
    var s = d.getSeconds();
    var m = d.getMinutes();
    var h = d.getHours();
    span.textContent = 
        ("0" + h).substr(-2) + ":" + ("0" + m).substr(-2) + ":" + ("0" + s).substr(-2);
    }

    time()

    setInterval(time, 1000);
</script>