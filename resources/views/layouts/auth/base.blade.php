<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        @yield('css')

        <title>@yield('title', 'ZuydPresence')</title>

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    </head>
    <body>
        <div class="h-full min-h-screen w-full min-w-full bg-gray-50 flex overflow-x-hidden">

            @yield('content')

        </div>

        @yield('js')
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>