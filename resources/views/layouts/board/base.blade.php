<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        @yield('css')

        <title>@yield('title', 'ZuydPresence')</title>

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">

        <meta http-equiv="refresh" content="15">
    </head>
    <body class="h-screen">
        <div class="min-w-full bg-gray-50 min-h-full">

            @include('components.board.ui.navigation')

        <main class="min-h-full">
            <div class="max-w-7xl mx-auto pt-6 sm:px-6 lg:px-8 min-h-full">

                <div class="px-4 sm:px-0 min-h-full">
                    @yield('content')
                </div>

            </div>
        </main>

        </div>

        @yield('js')
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
