<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        @yield('css')

        <title>@yield('title', 'ZuydPresence')</title>

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    </head>
    <body>
        <div class="h-full min-h-screen bg-gray-50 flex overflow-x-hidden">
            @include('components.dashboard.ui.sidenav')
            @include('components.dashboard.ui.sidenav_mobile')

            <div class="flex-1 flex flex-col">
                @include('components.dashboard.ui.searchbar')

                @yield('top_displaced')

                <main class="flex-1 p-5">
                    <section aria-labelledby="primary-heading" class="min-w-0 flex-1 h-full flex flex-col overflow-y-visible lg:order-last">
                        <h1 id="primary-heading" class="sr-only">Dashboard</h1>

                        @include('errors.alerts')

                        @yield('content')
                    </section>
                </main>
            </div>
        </div>

        @yield('js')
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>