{{-- All auth pages extend the global auth layout. --}}
@extends('layouts.auth.base')

{{-- Component. --}}
@section('content')
<form action="{{ route('login.submit') }}" method="POST" class="min-h-screen min-w-full w-full">

    @csrf

    <div class="min-h-screen min-w-full w-full bg-gray-100 flex flex-col justify-center sm:py-12">
        <div class="p-10 xs:p-0 mx-auto md:w-full md:max-w-md">
            <h1 class="font-bold text-center text-2xl mb-5">{{ __('nav.login') }}</h1>  
            <div class="bg-white shadow w-full rounded-lg divide-y divide-gray-200">
                <div class="px-5 py-7">
                    <label class="font-semibold text-sm text-gray-600 pb-1 block">{{ __('auth.email') }}</label>
                    <input name="email" type="text" class="border rounded-lg px-3 py-2 mt-1  text-sm w-full" />

                    @error('email')
                        <span class="text-red-400 text-sm -mt-5">
                            {{ $message }}
                        </span>
                    @enderror

                    <label class="font-semibold text-sm text-gray-600 pb-1 block mt-5">{{ __('auth.password') }}</label>
                    <input name="password" type="password" class="border rounded-lg px-3 py-2 mt-1 text-sm w-full" />

                    @error('password')
                        <span class="text-red-400 text-sm -mt-5">
                            {{ $message }}
                        </span>
                    @enderror
                    
                    <div class="mb-4 mt-4">
                        <div class="grid grid-cols-2 gap-1">
                            <div class="text-center sm:text-left whitespace-nowrap">
                                <a class="transition duration-200 py-4 cursor-pointer font-normal text-sm rounded-lg text-gray-500 focus:ring-gray-400 focus:ring-opacity-50 ring-inset">
                                    <input name="remember-me" type="checkbox"> <label for="remember">{{ __('auth.remember_me') }}</label>
                                </a>
                            </div>
                            <div class="text-center sm:text-right whitespace-nowrap">
                                <a class="transition duration-200 py-4 cursor-pointer font-normal text-sm rounded-lg text-gray-500 focus:ring-gray-400 focus:ring-opacity-50 ring-inset">
                                    <span class="inline-block ml-1 text-indigo-700">{{ __('auth.forgot') }}</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <input value="{{ __('nav.login') }}" type="submit" class="transition duration-200 bg-indigo-500 hover:bg-indigo-600 focus:bg-indigo-700 focus:shadow-sm focus:ring-4 focus:ring-indigo-500 focus:ring-opacity-50 text-white w-full py-2.5 rounded-lg text-sm shadow-sm hover:shadow-md font-semibold text-center inline-block" />
                </div>
            </div>
        </div>
    </div>
</form>
@stop