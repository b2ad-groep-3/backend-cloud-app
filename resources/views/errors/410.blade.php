{{--

    All error pages extend the global error page layout.
    This layout is defined at resources/layouts/errors/base.blade.php.

--}}

@extends('layouts.errors.base')

@section('error_number', '410')
@section('error_tagline', __('status_codes.410.title'))
@section('error_description', __('status_codes.410.description'))
