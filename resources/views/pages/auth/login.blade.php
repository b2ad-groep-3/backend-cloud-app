{{-- All auth pages extend the global auth layout. --}}
@extends('layouts.auth.base')

{{-- Component. --}}
@section('content')
<div class="min-h-screen min-w-full bg-gray-100 flex flex-col justify-center sm:py-12">
    <div class="p-10 xs:p-0 mx-auto md:w-full md:max-w-md">
        <h1 class="font-bold text-center text-2xl mb-5">{{ __('nav.login') }}</h1>  
        <div class="bg-white shadow w-full rounded-lg divide-y divide-gray-200">
            <div class="px-5 py-7">
                <label class="font-semibold text-sm text-gray-600 pb-1 block">{{ __('auth.email') }}</label>
                <input type="text" class="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full" />

                <label class="font-semibold text-sm text-gray-600 pb-1 block">{{ __('auth.password') }}</label>
                <input type="text" class="border rounded-lg px-3 py-2 mt-1 mb-5 text-sm w-full" />
                
                <div class="mb-4">
                    <div class="grid grid-cols-2 gap-1">
                        <div class="text-center sm:text-left whitespace-nowrap">
                            <a class="transition duration-200 py-4 cursor-pointer font-normal text-sm rounded-lg text-gray-500 focus:ring-gray-400 focus:ring-opacity-50 ring-inset">
                                <input name="remember" type="checkbox"> <label for="remember">{{ __('auth.remember_me') }}</label>
                            </a>
                        </div>
                        <div class="text-center sm:text-right whitespace-nowrap">
                            <a class="transition duration-200 py-4 cursor-pointer font-normal text-sm rounded-lg text-gray-500 focus:ring-gray-400 focus:ring-opacity-50 ring-inset">
                                <span class="inline-block ml-1 text-indigo-700">{{ __('auth.forgot') }}</span>
                            </a>
                        </div>
                    </div>
                </div>

                <button type="button" class="transition duration-200 bg-indigo-500 hover:bg-indigo-600 focus:bg-indigo-700 focus:shadow-sm focus:ring-4 focus:ring-indigo-500 focus:ring-opacity-50 text-white w-full py-2.5 rounded-lg text-sm shadow-sm hover:shadow-md font-semibold text-center inline-block">
                    <span class="inline-block mr-2">{{ __('nav.login') }}</span>
                </button>
            </div>
        </div>
    </div>
</div>
@stop