@extends('layouts.board.base')

@php
    $index = 0;
@endphp

@section('branch')
    {{ $branch }}
@stop

@section('content')

    @if($statuses->count() > 0)
        <div class="section w-full p-0">
            
            @foreach($statuses as $status)
                    
                {{-- If we are on the initial, or a 3rd status, create a new row. --}}
                @if($index == 0 || $index % 3 == 0)
                    <div class="grid grid-cols-3 gap-6 mt-3 mb-3">
                @endif

                    <div class="md:col-span-1 xl:col-span-1 bg-white rounded-lg shadow-lg p-3 col-span-3">
                        <div class="flex items-center">
                            <div class="grid grid-cols-12">
                                <div class="col-span-11 flex items-center">
                                    <img class="w-10 h-10 rounded-full mr-4" src="{{ asset('img/default_user.jpg') }}" alt="Avatar of Jonathan Reinink">
                                    <div class="text-sm">
                                        <p class="text-gray-900 leading-none">{{ $status->teacher->first_name }} {{ $status->teacher->middle_name }} {{ $status->teacher->last_name }}</p>
                                        <p class="text-gray-600">{{ __('zuydpresence.last_seen') }} {{ $status->updated_at->diffForHumans() }}</p>
                                    </div>
                                </div>
                                <div class="col-span-1 align-middle flex-col justify-center hidden lg:flex">
                                    @if($status->present)
                                        <div class="blob green"></div>
                                    @else
                                        <div class="blob red"></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    
                @php
                    $index++;
                @endphp

                {{-- If we are on the initial, or a 3rd status, close the row. --}}
                @if($index == 0 || $index % 3 == 0)
                    </div>
                @endif

            @endforeach

        </div>
    @else
        <div class="min-h-full min-w-full flex items-center justify-center" style="margin-top: 20%">
            <div class="empty-state text-center">
                <div class="empty-state-icon text-gray-400">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                    </svg>
                </div>
                <div class="empty-state-title text-gray-900">
                    {{ __('zuydpresence.no_teachers_present') }}
                </div>
                <div class="empty-state-text text-gray-600">
                    {{ __('zuydpresence.check_back') }}.
                </div>
            <div>
        </div>
    @endif

@stop