@extends('layouts.dashboard.base')

@section('content')
    <div>
        <h3 class="text-lg leading-6 font-medium text-gray-900 mb-5">
            {{ __('nav.settings') }}
        </h3>

        <div class="grid grid-cols-2 gap-6">

            <div class="col-span-2">
                <div class="bg-white rounded-lg shadow-lg px-4 py-5">
                    <div class="text-lg font-semibold capitalize">
                        {{ __('zuydpresence.account_settings') }}
                    </div>
                    <form method="POST" action="{{ route('settings.update') }}">

                        @csrf

                        <div class="text-sm font-medium text-gray-500 mb-2 mt-5">
                            {{ __('zuydpresence.name') }}
                        </div>
                        <input value="{{ auth()->user()->name }}" type="text" name="name" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.name') }}">
                        <div class="text-sm font-medium text-gray-500 mb-2 mt-5">
                            {{ __('zuydpresence.email') }}
                        </div>
                        <input value="{{ auth()->user()->email }}" type="text" name="email" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="{{ __('zuydpresence.email') }}">
                        <div class="text-sm font-medium text-gray-500 mb-2 mt-5">
                            {{ __('zuydpresence.language') }}
                        </div>
                        <select name="locale" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                            @foreach (config('zuydpresence.supported_locales') as $locale => $code)
                                <option {{ $code == app()->getLocale() ? 'selected' : '' }} value="{{ $code }}">{{ $locale }}</option>
                            @endforeach
                        </select>
                        <div class="text-right mt-5">
                            <input value="{{ __('zuydpresence.save') }}" type="submit" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@stop