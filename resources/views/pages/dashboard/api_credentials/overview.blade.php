{{-- All dashboard pages extend the global dashboard layout. --}}
@extends('layouts.dashboard.base')

{{-- Content. --}}
@section('content')

    @if(session()->has('credential'))
        @include('components.dashboard.modals.apikey_confirmation')
    @endif

    {{-- Include the statistics header in the content section. --}}
    @include('components.dashboard.tables.api_credentials')

@stop