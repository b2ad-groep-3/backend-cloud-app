{{-- All dashboard pages extend the global dashboard layout. --}}
@extends('layouts.dashboard.base')

{{-- Content. --}}
@section('content')

    {{-- Include the edit form in the content section. --}}
    @include('components.dashboard.forms.branch.create_form')

@stop