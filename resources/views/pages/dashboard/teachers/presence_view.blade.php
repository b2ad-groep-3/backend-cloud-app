{{-- All dashboard pages extend the global dashboard layout. --}}
@extends('layouts.dashboard.base')

{{-- Content. --}}
@section('content')

    {{-- Include the presence view in the content section. --}}
    @include('components.dashboard.tables.presence_view')

@stop