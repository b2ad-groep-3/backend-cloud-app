{{-- All dashboard pages extend the global dashboard layout. --}}
@extends('layouts.dashboard.base')

{{-- Content. --}}
@section('content')

    {{-- Include the single view in the content section. --}}
    @include('components.dashboard.forms.teacher.single_view')

@stop