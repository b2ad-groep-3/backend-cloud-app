{{-- All dashboard pages extend the global dashboard layout. --}}
@extends('layouts.dashboard.base')

{{-- Content. --}}
@section('content')

    {{-- Include the table in the content section. --}}
    @include('components.dashboard.tables.teachers')

@stop