{{-- All dashboard pages extend the global dashboard layout. --}}
@extends('layouts.dashboard.base')

{{-- Include the user header in the top displaced section. --}}
@section('top_displaced')
    @include('components.dashboard.widgets.user_header')
@stop

{{-- Content. --}}
@section('content')

    {{-- Include the statistics header in the content section. --}}
    @include('components.dashboard.widgets.all_time_statistics')

@stop