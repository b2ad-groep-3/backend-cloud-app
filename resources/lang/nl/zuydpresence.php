<?php

return [

    /*
    |--------------------------------------------------------------------------
    | ZuydPresence Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the ZuydPresence application for
    | all kinds of translations within the application.
    |
    */

    'create_new_teacher' => 'Nieuwe docent',
    'account_created' => 'Account aangemaakt',
    'teacher_count' => 'Docenten in systeem',
    'teacher_present_count' => 'Docenten aanwezig',
    'api_count' => 'Api verbindingen actief',
    'all_time_stats' => 'Sinds opzet',

    'api_credentials' => 'Api Credentials',
    'api_credential' => 'Api Credential',

    'api_key' => 'Api Sleutel',
    'secret' => 'Secret',
    'reference' => 'Referentie',
    'domain' => 'Domein',
    'domain_missing' => 'Geen domein gespecificeerd',
    'active' => 'Actief',
    'inactive' => 'Niet actief',
    'last_used_at' => 'Laatst gebruikt op',
    'created_at' => 'Aangemaakt op',
    'status' => 'Status',

    'edit' => 'Bewerk',
    'save' => 'Opslaan',
    'details' => 'gegevens',

    'successful_request' => 'Aanvraag succesvol',
    'resource_updated' => 'Resource is bijgewerkt.',
    'with_request' => 'met uw aanvraag',

    'error_found' => 'Er was|Er waren',
    'error' => 'fout|fouten',

    'report_bug' => 'Rapporteer bug',

    'create_apikey' => 'Nieuwe api key aanmaken',
    'create_apikey_confirmation' => 'De secret kan maar eenmalig weergeven worden. Als u deze kwijtraakt, zal u een nieuwe sleutel moeten genereren.',
    'apikey_details' => 'Gegevens van nieuwe api key',

    'confirm' => 'Bevestigen',
    'cancel' => 'Annuleren',

    'shown_after_creation' => 'Zal weergeven worden nadat de api-credential is aangemaakt',

    'create_branch' => 'Nieuwe vestiging maken',
    'create_teacher' => 'Nieuwe docent maken',

    'teacher' => 'Docent',

    'first_name' => 'Voornaam',
    'middle_name' => 'Tussenvoegsel',
    'last_name' => 'Achternaam',

    'name' => 'Naam',
    'email' => 'E-mail adres',
    'address' => 'Adresgegevens',
    'country' => 'Land',
    'branches' => 'Vestigingen',
    'branch' => 'Vestiging',
    'street' => 'Straat',
    'and' => 'en',
    'house_number' => 'Huisnummer',
    'postal_code' => 'Postcode',
    'city' => 'Stad',
    'state_province_county' => 'Staat / Provincie / County',

    'departments' => 'Afdelingen',
    'department' => 'Afdeling',
    'no_departments' => 'Deze vestiging heeft geen afdelingen',
    'has_departments' => 'Deze vestiging heeft de volgende afdelingen',
    'teacher_has_no_department' => 'Geen afdeling ingesteld',

    'present' => 'Aanwezig',
    'not_present' => 'Afwezig',
    'last_updated' => 'Laatst bijgewerkt op',

    'type' => 'Type',
    'group' => 'Groep',
    'severity' => 'Ernstigheid',
    'time' => 'Tijd',
    'entry' => 'Regel',
    'logs' => 'Logboeken',

    'additional_request_data' => 'Aanvullende data voor deze aanvraag',
    'no_additional_request_data' => 'Geen aanvullende data voor deze aanvraag',

    'view' => 'Bekijk',
    'photos' => 'Afbeeldingen',
    'no_photos' => 'Geen afbeeldingen.',

    'global_board' => 'Globaal aanwezigheidsbord',
    'management_application' => 'Management Applicatie',
    'global' => 'Globaal',
    'last_seen' => 'Laatst gezien',
    'no_teachers_present' => 'Geen docenten aanwezig',
    'check_back' => 'Kom later terug',

    'language' => 'Taal',

    'failed_request' => 'Aanvraag mislukt',
    'branch_has_teachers' => 'Deze vestiging kan niet worden verwijderd omdat er nog docenten aan gekoppeld zijn',
    
    'account_settings' => 'Account instellingen'
];
