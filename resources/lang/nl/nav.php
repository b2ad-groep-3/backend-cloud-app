<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the navigation bars.
    |
    */

    'dashboard' => 'Dashboard',
    'teachers' => 'Docenten',
    'presence' => 'Aanwezigheid',
    'settings' => 'Instellingen',
    'help' => 'Hulp',
    'search' => 'Zoeken',
    'logout' => 'Uitloggen',
    'login' => 'Inloggen',
    'profile' => 'Mijn profiel',
    'close' => 'Sluiten',
    'api' => 'Api',
    'logs' => 'Logboeken'
];