<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Status Codes Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for HTTP status codes that are 
    | displayed in the application. These can be modified and localized for all 
    | the languages that the ZuydPresence app supports.
    |
    */

    '400' => [
        'title' => 'Foute Aanvraag',
        'description' => 'De server zal deze aanvraag niet verwerken.'
    ],

    '401' => [
        'title' => 'Niet geautoriseerd',
        'description' => 'Authenticatie heeft nog niet of onjuist plaatsgevonden.'
    ],
    
    '402' => [
        'title' => 'Betaalde Toegang',
        'description' => 'Een betaling is nodig om door te gaan.'
    ],

    '403' => [
        'title' => 'Verboden toegang',
        'description' => 'U heeft geen toegang om deze pagina te bekijken.'
    ],

    '404' => [
        'title' => 'Niet Gevonden',
        'description' => 'Deze pagina is niet gevonden.'
    ],

    '405' => [
        'title' => 'Methode Niet Toegestaan',
        'description' => 'De methode die u gebruikt is niet toegestaan voor deze pagina.'
    ],

    '408' => [
        'title' => 'Aanvraagtijd verstreken',
        'description' => 'De gebruiker heeft geen aanvraag gedaan binnen de gestelde tijd.'
    ],

    '410' => [
        'title' => 'Verdwenen',
        'description' => 'Deze pagina is niet meer beschikbaar.'
    ],

    '413' => [
        'title' => 'Aanvraag Te Groot',
        'description' => 'De aanvraag is te groot en de server zal deze niet verwerken.'
    ],

    '429' => [
        'title' => 'Teveel Aanvragen',
        'description' => 'U heeft teveel aanvragen verstuurd.'
    ],

    '500' => [
        'title' => 'Interne Serverfout',
        'description' => 'De server kon uw aanvraag niet verwerken.'
    ],

    '501' => [
        'title' => 'Niet Geïmplementeerd',
        'description' => 'De server kan niet aan deze aanvraag voldoen.'
    ],

    '502' => [
        'title' => 'Bad Gateway',
        'description' => 'De server heeft een ongeldig antwoord ontvangen van de upstream server.'
    ],

    '503' => [
        'title' => 'Dienst niet beschikbaar',
        'description' => 'De server kan deze aanvraag niet behandelen (overbelast of in verband met onderhoud).'
    ],

    '504' => [
        'title' => 'Gateway Timeout',
        'description' => 'De server heeft geen antwoord ontvangen van de upstream server.'
    ],

    '505' => [
        'title' => 'HTTP Version Wordt Niet Ondersteund',
        'description' => 'De server ondersteund de gebruikte HTTP-versie niet.'
    ],

];
