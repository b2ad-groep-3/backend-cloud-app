<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Status Codes Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for HTTP status codes that are 
    | displayed in the application. These can be modified and localized for all 
    | the languages that the ZuydPresence app supports.
    |
    */

    '400' => [
        'title' => 'Bad Request',
        'description' => 'The server will not process this request.'
    ],

    '401' => [
        'title' => 'Unauthorized',
        'description' => 'Authentication has not yet been provided or is invalid.'
    ],
    
    '402' => [
        'title' => 'Payment Required',
        'description' => 'A payment is required to continue.'
    ],

    '403' => [
        'title' => 'Forbidden',
        'description' => 'You do not have permission to access this page or resource.'
    ],

    '404' => [
        'title' => 'Not Found',
        'description' => 'This page was not found on the server.'
    ],

    '405' => [
        'title' => 'Method Not Allowed',
        'description' => 'The method you requested is not allowed for this page.'
    ],

    '408' => [
        'title' => 'Request Timeout',
        'description' => 'The client did not produce a request in time.'
    ],

    '410' => [
        'title' => 'Gone',
        'description' => 'This page or resource is no longer available.'
    ],

    '413' => [
        'title' => 'Payload Too Large',
        'description' => 'The request is larger than the server is willing to process.'
    ],

    '429' => [
        'title' => 'Too Many Requests',
        'description' => 'You have sent too many requests.'
    ],

    '500' => [
        'title' => 'Internal Server Error',
        'description' => 'The server was unable to process your request.'
    ],

    '501' => [
        'title' => 'Not Implemented',
        'description' => 'The server cannot fufil this request.'
    ],

    '502' => [
        'title' => 'Bad Gateway',
        'description' => 'The server received an invalid response from the upstream server.'
    ],

    '503' => [
        'title' => 'Service Unavailable',
        'description' => 'The server cannot handle the request (it is overloaded or down for maintenance).'
    ],

    '504' => [
        'title' => 'Gateway Timeout',
        'description' => 'The server did not receive a timely response from the upstream server.'
    ],

    '505' => [
        'title' => 'HTTP Version Not Supported',
        'description' => 'The server does not support the HTTP version used in the request.'
    ],

];
