<?php

return [

    /*
    |--------------------------------------------------------------------------
    | ZuydPresence Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the ZuydPresence application for
    | all kinds of translations within the application.
    |
    */

    'create_new_teacher' => 'Create new teacher',
    'account_created' => 'Account created',
    'teacher_count' => 'Teachers in the system',
    'teacher_present_count' => 'Teachers present',
    'api_count' => 'Active Api connections',
    'all_time_stats' => 'All-time statistics',

    'api_credentials' => 'Api Credentials',
    'api_credential' => 'Api Credential',

    'api_key' => 'Api Key',
    'secret' => 'Secret',
    'reference' => 'Reference',
    'domain' => 'Domain',
    'domain_missing' => 'No domain specified',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'last_used_at' => 'Last used at',
    'created_at' => 'Created at',
    'status' => 'Status',

    'edit' => 'Edit',
    'save' => 'Save',
    'details' => 'details',

    'successful_request' => 'Request successful',
    'resource_updated' => 'Resource has been updated.',
    'with_request' => 'with your request',

    'error_found' => 'There was|There were',
    'error' => 'error|errors',

    'report_bug' => 'Report bug',

    'create_apikey' => 'Create an api key',
    'create_apikey_confirmation' => 'The secret can only be shown once. If you lose it, you will need to create a new api key.',
    'apikey_details' => 'Details of new api key',

    'confirm' => 'Confirm',
    'cancel' => 'Cancel',

    'shown_after_creation' => 'Will be shown after creating the api credential',

    'create_branch' => 'Create a branch',
    'create_teacher' => 'Create a new teacher',

    'teacher' => 'Teacher',

    'first_name' => 'First name',
    'middle_name' => 'Middle name',
    'last_name' => 'Last name',

    'name' => 'Name',
    'email' => 'Email address',
    'address' => 'Address',
    'country' => 'Country',
    'branches' => 'Branches',
    'branch' => 'Branch',
    'street' => 'Street',
    'and' => 'and',
    'house_number' => 'House number',
    'postal_code' => 'Postal code',
    'city' => 'City',
    'state_province_county' => 'State / Province / County',

    'departments' => 'Departments',
    'department' => 'Department',
    'no_departments' => 'This branch does not have any departments',
    'has_departments' => 'This branch has the following departments',
    'teacher_has_no_department' => 'No department specified',

    'present' => 'Present',
    'not_present' => 'Not present',
    'last_updated' => 'Last updated at',

    'type' => 'Type',
    'group' => 'Group',
    'severity' => 'Severity',
    'time' => 'Time',
    'entry' => 'Entry',
    'logs' => 'Logs',

    'additional_request_data' => 'Additional data for this request',
    'no_additional_request_data' => 'No additional data for this request',

    'view' => 'View',
    'photos' => 'Photos',
    'no_photos' => 'No photos.',

    'global_board' => 'Global presence board',
    'management_application' => 'Management Application',
    'global' => 'Global',
    'last_seen' => 'Last seen',
    'no_teachers_present' => 'No teachers present',
    'check_back' => 'Come back later',

    'language' => 'Language',

    'failed_request' => 'Request failed',
    'branch_has_teachers' => 'You cannot delete this branch because it still has teachers attached',

    'account_settings' => 'Account settings'
];
