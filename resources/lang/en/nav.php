<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the navigation bars.
    |
    */

    'dashboard' => 'Dashboard',
    'teachers' => 'Teachers',
    'presence' => 'Presence',
    'settings' => 'Settings',
    'help' => 'Help',
    'search' => 'Search',
    'logout' => 'Logout',
    'login' => 'Log in',
    'profile' => 'My profile',
    'close' => 'Close sidebar',
    'api' => 'Api',
    'logs' => 'Logs'
];