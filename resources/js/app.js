require('./bootstrap');

mobile_sidebar = document.getElementById('mobile-sidebar');
mobile_sidebar_open_button = document.getElementById('mobile-sidebar-open-button');
mobile_sidebar_close_button = document.getElementById('mobile-sidebar-close-button');

// We hide the mobile menu by default.
mobile_sidebar.classList.add('invisible');

// Event handler for when a user clicks on the button
// to open the mobile side menu.
mobile_sidebar_open_button.addEventListener('click', function () {
    
    // If we are coming from an invisible state, show the menu.
    if(mobile_sidebar.classList.contains('invisible')) {
        mobile_sidebar.classList.remove('invisible')
        mobile_sidebar.classList.add('transition ease-in-out duration-300 transform -translate-x-full')
    }

});

mobile_sidebar_close_button.addEventListener('click', function () {
    
    // If we are coming from a visible state, hide the menu.
    if(!mobile_sidebar.classList.contains('invisible')) {
        mobile_sidebar.classList.add('invisible')
        mobile_sidebar.classList.add('transition ease-in-out duration-300 transform -translate-x-full')
    }

});

dropdown_open_button = document.getElementById('dropdown-open-button');
dropdown_content = document.getElementById('dropdown-content');

dropdown_content.classList.add('invisible');

document.addEventListener('click', function (event) {

    if(dropdown_content.classList.contains('invisible') && (event.target.parentElement.id == dropdown_open_button.id)) {
        dropdown_content.classList.remove('invisible')
    } else {
        dropdown_content.classList.add('invisible')
    }

});