<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\LogsController;
use App\Http\Controllers\LocaleController;
use App\Http\Controllers\BranchesController;
use App\Http\Controllers\TeachersController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PresenceBoardController;
use App\Http\Controllers\ApiCredentialsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Redirect all base requests to the presence board.
// If a user is not authenticated, the middleware will redirect them.
Route::get('/', function () {
    return redirect()->route('presenceboard.global');
});

/**
 * These are the application related routes. This includes the
 * management application and all related URLs. This group implements
 * the auth middleware to ensure only authenticated users can access them.
 */
Route::prefix('app')->middleware(['auth'])->group(function (){

    Route::get('dashboard', [DashboardController::class, 'dashboard'])->name('dashboard.home');

    /**
     * These are the routes that belong to the api credential resource.
     * They are all prefixed by 'api-credentials'.
     */
    Route::prefix('api-credentials')->group(function () {
        Route::get('overview', [ApiCredentialsController::class, 'index'])->name('apicredentials.overview');
        Route::get('create', [ApiCredentialsController::class, 'create'])->name('apicredentials.create');
        Route::get('{api_credential}/view', [ApiCredentialsController::class, 'show'])->name('apicredentials.view');
        Route::get('{api_credential}/edit', [ApiCredentialsController::class, 'edit'])->name('apicredentials.edit');

        Route::post('store', [ApiCredentialsController::class, 'store'])->name('apicredentials.store');
        Route::post('{api_credential}/update', [ApiCredentialsController::class, 'update'])->name('apicredentials.update');
        Route::delete('{id}/delete', [ApiCredentialsController::class, 'destroy'])->name('apicredentials.delete');
    });

    /**
     * These are the routes that belong to the branch resource.
     * They are all prefixed by 'branches'.
     */
    Route::prefix('branches')->group(function () {
        Route::get('overview', [BranchesController::class, 'index'])->name('branches.overview');
        Route::get('create', [BranchesController::class, 'create'])->name('branches.create');
        Route::get('{branch}/view', [BranchesController::class, 'show'])->name('branches.view');
        Route::get('{branch}/edit', [BranchesController::class, 'edit'])->name('branches.edit');

        Route::post('store', [BranchesController::class, 'store'])->name('branches.store');
        Route::post('{branch}/update', [BranchesController::class, 'update'])->name('branches.update');
        Route::delete('{id}/delete', [BranchesController::class, 'destroy'])->name('branches.delete');
    });

    /**
     * These are the routes that belong to the teacher resource.
     * They are all prefixed by 'teachers'.
     */
    Route::prefix('teachers')->group(function () {
        Route::get('overview', [TeachersController::class, 'index'])->name('teachers.overview');
        Route::get('create', [TeachersController::class, 'create'])->name('teachers.create');
        Route::get('{teacher}/view', [TeachersController::class, 'show'])->name('teachers.view');
        Route::get('{teacher}/edit', [TeachersController::class, 'edit'])->name('teachers.edit');
        Route::get('presence', [TeachersController::class, 'presence'])->name('teachers.presence');
        Route::get('branch/{branch}', [TeachersController::class, 'focus'])->name('teachers.focus');

        Route::post('filter', [TeachersController::class, 'filter'])->name('teachers.filter');
        Route::post('store', [TeachersController::class, 'store'])->name('teachers.store');
        Route::post('{teacher}/update', [TeachersController::class, 'update'])->name('teachers.update');
        Route::delete('{id}/delete', [TeachersController::class, 'destroy'])->name('teachers.delete');
    });

    /**
     * These are the routes used to display logs.
     * They are all prefixed by 'logs'.
     */
    Route::prefix('logs')->group(function () {
        Route::get('/', [LogsController::class, 'all'])->name('logs.all');
        Route::get('{group}', [LogsController::class, 'focus'])->name('logs.focus');

        Route::post('/', [LogsController::class, 'filter'])->name('logs.filter');
        Route::post('/clear', [LogsController::class, 'clear'])->name('logs.clear');
    });

    /**
     * These are the routes used to display settings.
     * They are all prefixes by 'settings'.
     */
    Route::prefix('settings')->group(function () {
        Route::get('/', [SettingsController::class, 'view'])->name('settings.view');

        Route::post('/update', [SettingsController::class, 'update'])->name('settings.update');
    });

});

/**
 * These are the routes for the public presence board.
 * 
 * Does not implement the auth middleware, since an account is not required.
 */
Route::prefix('presence-board')->group(function () {
    Route::get('/', [PresenceBoardController::class, 'global'])->name('presenceboard.global');
    Route::get('{id}', [PresenceBoardController::class, 'focus'])->name('presenceboard.focus');
});

/**
 * These are the authentication related routes.
 * They are responsible for handling login requests, resetting passwords and logouts.
 * 
 * Does not implement the auth middleware, since an account is not required.
 */
Route::prefix('app/auth')->group(function () {  
    Route::get('login', 'App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login.form');
    Route::post('login', 'App\Http\Controllers\Auth\LoginController@login')->name('login.submit');
    Route::post('logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('login.logout');

    Route::get('password/reset', 'App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm')->name('reset.requestform');
    Route::get('password/reset/{token}', 'App\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('reset.resetform');
    Route::post('password/email', 'App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('reset.submitrequest');
    Route::post('password/reset', 'App\Http\Controllers\Auth\ResetPasswordController@reset')->name('reset.submitreset');
});

Route::post('locale/set', [LocaleController::class, 'setLocaleThroughCookie'])->name('locale.save');