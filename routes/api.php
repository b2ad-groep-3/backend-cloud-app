<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('api')->middleware(['zuydpresence_api'])->group(function () {

    // Api version one.
    Route::prefix('v1')->group(function () {

        // Verification route that can be used to test api functionality.
        Route::get('verify', [ApiController::class, 'verify']);

        // All teachers in JSON format.
        Route::get('/teachers', [ApiController::class, 'teachers']);

        /**
         * API endpoints for the teacher resource.
         * These are prefixed by 'teacher'.
         */
        Route::prefix('teacher')->group(function () {
            Route::get('{id}', [ApiController::class, 'get_teacher_by_id']);

            Route::post('status/photo', [ApiController::class, 'update_status_by_photo_name']);
        });

        Route::get('images/database', [ApiController::class, 'export_image_database']);

    });

});