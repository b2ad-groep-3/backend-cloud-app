<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatedLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id();
            $table->mediumText('entry');
            $table->enum('severity', ['success', 'warning', 'info', 'error']);
            $table->enum('group', ['api', 'system', 'other']);
            $table->string('api_key')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->integer('status_code')->nullable();
            $table->string('ecode')->nullable();
            $table->string('signature')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
