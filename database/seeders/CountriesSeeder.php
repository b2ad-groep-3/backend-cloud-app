<?php

namespace Database\Seeders;

use File;
use App\Models\Country;
use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/countries.json");
        $countries = json_decode($json);

        foreach($countries as $key => $value)
        {
            Country::create([
                "code" => $key,
                "name" => $value
            ]);
        }
    }
}
