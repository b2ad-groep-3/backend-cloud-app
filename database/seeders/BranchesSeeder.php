<?php

namespace Database\Seeders;

use File;
use Carbon\Carbon;
use App\Models\Branch;
use Illuminate\Database\Seeder;

class BranchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/zuyd_branches.json");
        $branches = json_decode($json);

        foreach($branches as $branch)
        {
            Branch::create([
                "name" => $branch->name,
                "street" => $branch->street,
                "house_number" => $branch->house_number,
                "city" => $branch->city,
                "province_state_county" => $branch->province_state_county,
                "postal_code" => $branch->postal_code,
                "country_code" => $branch->country_code,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }
    }
}
