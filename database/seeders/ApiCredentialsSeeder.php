<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\ApiCredential;
use Illuminate\Database\Seeder;

class ApiCredentialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApiCredential::create([
            "key" => Str::random(64),
            "secret" => Str::random(64),
            "domain" => null,
            "activated" => false,
            "reference" => "Example Api Key",
            "last_used_at" => null,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
    }
}
