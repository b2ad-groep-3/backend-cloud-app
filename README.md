# ZuydPresence Management App

This is the management application for the ZuydPresence project. This management application will allow administrators to manage all aspects of the application, like:

- Creating API-keys
- Managing teachers
- Managing camera systems

## Setup

Download this repository via git.

```
$ git clone https://gitlab.com/b2ad-groep-3/backend-cloud-app.git
```

Install dependencies via composer and npm.

```
$ composer install
$ npm install
```

Compile the front-end assets

```
$ npm run dev
```

Set up a database connection in the `.env` file.

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=zuydpresence
DB_USERNAME=root
DB_PASSWORD=
```

Create and seed the database.

```
php artisan migrate:fresh --seed
```

You can now log in at `/app/auth/login` with these credentials.

```
username: admin@zuydpresence.local
password: zuydpresence
```