<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Locales
    |--------------------------------------------------------------------------
    |
    | ZuydPresence has the ability to localize the website for a number of
    | locales. The supported locales are defined here.
    |
    */

    'supported_locales' => [
        'English' => 'en',
        'Nederlands' => 'nl'
    ],

    /*
    |--------------------------------------------------------------------------
    | Exceptions
    |--------------------------------------------------------------------------
    |
    | ZuydPresence utilises a custom logging implementation to gather logs
    | about the api and the system. The exceptions for these log entries are
    | standardised. They are defined here.
    |
    */

    'exceptions' => [

        'missing_header' => [
            'status_code' => 401,
            'message' => 'Received API request without authorization header.',
            'ecode' => 'The authorization header was missing or invalid.'
        ],

        'malformed_header' => [
            'status_code' => 401,
            'message' => 'Received API request with malformed authorization header.',
            'ecode' => 'The provided authorization header was incomplete or in an incorrect format.'
        ],

        'nonexistent_apikey' => [
            'status_code' => 401,
            'message' => 'Received API request that attempted to use an invalid API key.',
            'ecode' => 'The provided API key is unknown to the server.'
        ],

        'inactive_apikey' => [
            'status_code' => 401,
            'message' => 'Received API request that attempted to use an unactivated API key.',
            'ecode' => 'The provided API key has been marked as inactive.'
        ],

        'method_mismatch' => [
            'status_code' => 401,
            'message' => 'Received API request with mismatching HTTP methods.',
            'ecode' => 'The HTTP method provided in the signature did not match that of the request.'
        ],

        'invalid_body' => [
            'status_code' => 400,
            'message' => 'Received API request with an invalid body.',
            'ecode' => 'The provided request body was not in JSON or in invalid JSON.'
        ],

        'signature_mismatch' => [
            'status_code' => 401,
            'message' => 'Received API request with mismatching signatures.',
            'ecode' => 'The provided signature did not match the reconstructed signature by the server.'
        ],

        'request_expired' => [
            'status_code' => 401,
            'message' => 'Received API request that expired.',
            'ecode' => 'The request has already been sent to the server before and passed the maximum time to live.'
        ],

    ],

];