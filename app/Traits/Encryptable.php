<?php

namespace App\Traits;

use Illuminate\Support\Facades\Crypt;

trait Encryptable
{
    /**
     * If the attribute is in the encryptable array, we need
     * to decrypt it and then continue as normal.
     * 
     * @param $key
     * 
     * @return $value
     */
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if(in_array($key, $this->encryptable) && $value !== '')
        {
            $value = Crypt::decrypt($value);
        }

        return $value;
    }

    /**
     * If the attribute is in the encryptable array, we need
     * to decrypt it and then continue as normal.
     * 
     * @param $key
     * 
     * @return $value
     */
    public function setAttribute($key, $value)
    {
        if(in_array($key, $this->encryptable))
        {
            $value = Crypt::encrypt($value);
        }

        return parent::setAttribute($key, $value);
    }
}