<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
     * Departments require timestamps for administrative purposes.
     * They are fully user created.
     */
    public $timestamps = true;

    /**
     * Some attributes of the departments are editable. They
     * are assigned to the fillable array to allow mass assignment.
     */
    protected $fillable = ['name', 'branch_id'];
}
