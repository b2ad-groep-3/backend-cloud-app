<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /**
     * Statuses require timestamps for administrative purposes.
     * These can be shown on the presence boards.
     */
    public $timestamps = true;

    /**
     * Some attributes of the statuses are editable. They
     * are assigned to the fillable array to allow mass assignment.
     */
    protected $fillable = ['present', 'branch_id', 'teacher_id'];

    /**
     * The table name does not match the standard Laravel convention.
     * We have to define it here.
     */
    public $table = 'teacher_statuses';

    /**
     * A status belongs to a teacher. This relationship is defined here.
     * Returns an instance of the teacher that the status belongs to.
     */
    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
    }

    /**
     * A status has a branch, to indicate where the teacher currently is.
     * Returns an instance of the branch that the teacher is at.
     */
    public function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }

    /**
     * Scope a query to only include present statuses.
     * 
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return void
     */
    public function scopePresent($query)
    {
        $query->where('present', true);
    }
}
