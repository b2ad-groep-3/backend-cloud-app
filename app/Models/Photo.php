<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * A photo belongs to a teacher.
     * This relationship returns an instance of the teacher.
     */
    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    /**
     * Some attributes of the photos are editable. They
     * are assigned to the fillable array to allow mass assignment.
     */
    protected $fillable = ['name', 'teacher_id'];

    /**
     * Photos do not require timestamps since they cannot be edited.
     * Disable them here.
     */
    public $timestamps = false;
}
