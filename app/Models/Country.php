<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * Countries are seeded and do not offer crud operations.
     * We do not need creation and update timestamps.
     */
    public $timestamps = false;

    /**
     * The primary key is the ISO-3166-1 alpha 2 code. We do not
     * need auto-increment since it is a two character string.
     */
    public $incrementing = false;

    /**
     * We are on Laravel > 6.0.0, so we have to explicitly set
     * the key type because it isn't an integer.
     */
    protected $keyType = "string";

    /**
     * The primary key does not match the standard 'id' value,
     * we must define the custom name of the primary key here.
     */
    protected $primaryKey = "code";
}
