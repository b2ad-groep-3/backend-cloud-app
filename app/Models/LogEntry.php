<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogEntry extends Model
{
    /**
     * Logs require timestamps for administrative purposes.
     * These can be shown on the log overview.
     */
    public $timestamps = true;

    /**
     * Some attributes of the logentries are editable. They
     * are assigned to the fillable array to allow mass assignment.
     */
    protected $fillable = ['entry', 'severity', 'group', 'api_key', 'ecode', 'user_id', 'status_code', 'signature'];

    /**
     * The table name does not match the standard Laravel convention.
     * We have to define it here.
     */
    public $table = 'logs';
}
