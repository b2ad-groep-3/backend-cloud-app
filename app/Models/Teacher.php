<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    /**
     * Teachers require timestamps for administrative purposes.
     * They are fully user created.
     */
    public $timestamps = true;

    /**
     * Some attributes of the departments are editable. They
     * are assigned to the fillable array to allow mass assignment.
     */
    protected $fillable = ['first_name', 'middle_name', 'last_name', 'email', 'department_id', 'branch_id'];

    /**
     * Some of the attributes require a default value.
     * Those defaults are defined here.
     */
    protected $attributes = [
        'department_id' => null
    ];

    /**
     * Teachers belong to one branch. This relationship is defined here.
     * Returns an instance of the branch that the teacher belongs to.
     */
    public function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }

    /**
     * Teachers belong to one department. This relationship is defined here.
     * Returns an instance of the department that the teacher belongs to.
     */
    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    /**
     * A teacher has one or more photos. This relationship is defined here.
     * Returns a collection of all photo's that belong to the teacher.
     */
    public function photos()
    {
        return $this->hasMany(Photo::class, 'teacher_id');
    }
}
