<?php

namespace App\Models;

use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;

class ApiCredential extends Model
{
    use Encryptable;

    /**
     * Branches require timestamps for administrative purposes.
     * They are partially seeded, partially user-added.
     */
    public $timestamps = true;

    /**
     * The credentials for the API are sensitive information and
     * should not be stored in plaintext. Encrypt them using the
     * custom Encryptable trait.
     */
    protected $encryptable = ['secret'];

    /**
     * Some attributes of the API credentials are editable. They
     * are assigned to the fillable array to allow mass assignment.
     */
    protected $fillable = ['key', 'secret', 'reference', 'activated', 'domain'];
}
