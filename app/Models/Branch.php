<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    /**
     * Branches require timestamps for administrative purposes.
     * They are partially seeded, partially user-added.
     */
    public $timestamps = true;

    /**
     * Branches belong to a country. That relationship is defined
     * here and returns the country that a branch belongs to.
     */
    public function country()
    {
        return $this->hasOne(Country::class, 'code', 'country_code');
    }

    /**
     * Branches can have multiple departments. That relationship
     * is defined here and returns a collection of departments.
     */
    public function departments()
    {
        return $this->hasMany(Department::class, 'branch_id', 'id');
    }

    /**
     * Some attributes of the branches are editable. They
     * are assigned to the fillable array to allow mass assignment.
     */
    protected $fillable = ['name', 'street', 'house_number', 'province_state_county', 'postal_code', 'city', 'country_code'];
}
