<?php

namespace App\Helpers;

use App\Models\LogEntry;

class ZuydPresenceLog
{
    /**
     * Log an exception that occured in the API.
     * 
     * @param string $exception
     * @param string $api_key
     * @param string $signature
     * @param int $user_id
     * 
     * @return void
     */
    public static function apiException(string $exception, string $api_key = null, string $signature = null, int $user_id = null)
    {
        $possible_exceptions = array_keys(config('zuydpresence.exceptions'));

        if(!in_array($exception, $possible_exceptions))
        {
            return 'Exception type invalid. Please see config/zuydpresence.php.';
        }

        $description = config('zuydpresence.exceptions')[$exception];

        LogEntry::create([
            'severity' => 'error',
            'group' => 'api',
            'entry' => $description['message'],
            'ecode' => $description['ecode'],
            'status_code' => $description['status_code'],
            'api_key' => $api_key,
            'signature' => $signature,
            'user_id' => $user_id
        ]);
    }

    /**
     * Get a list of valid exceptions for the application.
     * 
     * @return array
     */
    private function getExceptions()
    {
        return array_keys(config('zuydpresence.exceptions'));
    }
}