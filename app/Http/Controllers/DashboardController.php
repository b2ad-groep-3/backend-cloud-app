<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Models\ApiCredential;

class DashboardController extends Controller
{
    /**
     * Returns an instance of the management application's
     * dashboard.
     */
    public function dashboard()
    {
        $statistics = [
            'teacher_count' => Teacher::count(),
            'present_count' => Status::where('present', true)->count(),
            'active_api_count' => ApiCredential::where('activated', true)->count()
        ];

        return view('pages.dashboard.home')
            ->with('statistics', $statistics);
    }
}
