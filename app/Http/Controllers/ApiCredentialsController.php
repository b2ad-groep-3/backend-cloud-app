<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ApiCredential;
use App\Http\Requests\ApiCredentialStoreRequest;
use App\Http\Requests\ApiCredentialUpdateRequest;

class ApiCredentialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $api_credentials = ApiCredential::paginate(10);

        return view('pages.dashboard.api_credentials.overview')
            ->with('api_credentials', $api_credentials);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.dashboard.api_credentials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApiCredentialStoreRequest $request)
    {
        $validated = $request->validated();

        if($validated["activated"] == 'active')
        {
            $validated["activated"] = true;
        }
        else
        {
            $validated["activated"] = false;
        }

        $validated['key'] = uniqid('zp');
        $validated['secret'] = Str::random(64);

        $credential = ApiCredential::create($validated);

        return redirect()
            ->route('apicredentials.overview')
            ->with('credential', $credential)
            ->with('success', __('zuydpresence.resource_updated'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ApiCredential $api_credential)
    {
        return view('pages.dashboard.api_credentials.view')
            ->with('api_credential', $api_credential);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ApiCredential $api_credential)
    {
        return view('pages.dashboard.api_credentials.edit')
            ->with('api_credential', $api_credential);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ApiCredentialUpdateRequest $request, ApiCredential $api_credential)
    {
        $updated = $request->validated();

        if($updated["activated"] == 'active')
        {
            $updated["activated"] = true;
        }
        else
        {
            $updated["activated"] = false;
        }

        $api_credential->update($updated);

        return redirect()
            ->route('apicredentials.overview')
            ->with('success', __('zuydpresence.resource_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $credential = ApiCredential::find($id);

        $credential->delete();

        return redirect()
            ->route('apicredentials.overview')
            ->with('success', __('zuydpresence.resource_updated'));
    }
}
