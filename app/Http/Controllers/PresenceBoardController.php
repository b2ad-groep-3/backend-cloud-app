<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Models\Branch;
use Illuminate\Http\Request;

class PresenceBoardController extends Controller
{
    /**
     * Show all the statuses.
     * 
     * @return Illuminate\Http\Response
     */
    public function global()
    {
        // We get the present people and group them per branch.
        // The present scope is defined within the Status model.
        $branches = Status::present()->with('branch', 'teacher')->get()->sortBy('teacher.first_name')->groupBy('branch.name');

        $filters = Branch::select('id', 'name')->get();

        return view('pages.board.global')
            ->with('branches', $branches)
            ->with('filters', $filters);
    }

    /**
     * Show the statuses for a branch.
     * 
     * @return Illuminate\Http\Response
     */
    public function focus($id)
    {
        if(!Branch::where('id', $id)->exists())
        {
            abort(404);
        }

        $branch = Branch::select('name')->where('id', $id)->first()['name'];
        $statuses = Status::where('branch_id', $id)->orderBy('present', 'desc')->get();
        $filters = Branch::select('id', 'name')->get();

        return view('pages.board.focused')
            ->with('statuses', $statuses)
            ->with('branch', $branch)
            ->with('filters', $filters);
    }
}
