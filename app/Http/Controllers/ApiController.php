<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\Status;
use App\Models\Branch;
use App\Models\Teacher;
use App\Models\LogEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{
    /**
     * This function allows API-users to test their requests by sending one to
     * the verify endpoint. The middleware set will verify the HMAC-signature.
     * 
     * If we get here, the HMAC-signature is valid so we can simply return a response.
     * 
     * @return Illuminate\Http\Response
     */
    public function verify()
    {
        LogEntry::create([
            'severity' => 'success',
            'group' => 'api',
            'entry' => 'Api received a valid hmac verification request.',
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Request has been received.'
        ], 200);
    }

    /**
     * Return all teachers in the system.
     * 
     * @return Illuminate\Http\Response
     */
    public function teachers()
    {
        $teachers = Teacher::all()->toJson();

        return response()->json([
            'status' => 'success',
            'data' => [
                'teachers' => $teachers
            ]
        ], 200);
    }

    /**
     * Update a status by a photo name.
     * 
     * @return Illuminate\Http\Response
     */
    public function update_status_by_photo_name(Request $request)
    {
        $body = json_decode($request->getContent(), true);

        // Check if we have all the required parameters.
        if(!isset($body['image']) || !isset($body['branch_id']) || !isset($body['present']))
        {
            return response()->json([
                'status' => 'fail',
                'data' => [
                    'msg' => 'Missing required parameter(s) (image, branch_id, present)'
                ]
            ], 400);
        }

        // Check if the photo and branch exist.
        if(!Photo::where('name', $body['image'])->exists() || !Branch::where('id', $body['branch_id'])->exists())
        {
            return response()->json([
                'status' => 'fail',
                'data' => [
                    'msg' => 'The photo or branch does not exist.'
                ]
            ], 400);
        }

        // Get the photo to retrieve the id of the teacher.
        $photo = Photo::where('name', $body['image'])->first();

        // Update the status.
        Status::where('teacher_id', $photo->teacher_id)->update(['branch_id' => $body['branch_id'], 'present' => $body['present']]);

        return response()->json([
            'status' => 'success',
            'message' => 'Status has been updated.'
        ], 200);
    }

    /**
     * Get a teacher by their id.
     * 
     * @return Illuminate\Http\Response
     */
    public function get_teacher_by_id(Request $request)
    {
        if(!Teacher::where('id', $request->route('id'))->exists())
        {
            return response()->json([
                'status' => 'error',
                'message' => 'A teacher with that ID does not exist.'
            ], 404);
        }

        $teacher = Teacher::find($request->route('id'));

        return response()->json([
            'status' => 'success',
            'data' => $teacher
        ], 200);
    }

    /**
     * Download the image database.
     * 
     * @return Illuminate\Http\Response
     */
    public function export_image_database()
    {
        $zip = new \ZipArchive();
        $zip_name = 'zuydpresence_image_export.zip';

        // Attempt to create the new zipfile.
        if($zip->open(public_path($zip_name), \ZipArchive::CREATE))
        {
            $files = File::files(public_path('uploads'));

            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }
             
            $zip->close();
        }
        else
        {
            abort(500);
        }

        // Move the zipfile into the storage.
        File::move(public_path($zip_name), storage_path('exports/zuydpresence_image_export.zip'));

        // Stream a download.
        return Storage::disk('exports')->download($zip_name);
    }
}
