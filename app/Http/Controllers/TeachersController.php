<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\Branch;
use App\Models\Status;
use App\Models\Teacher;
use App\Models\Department;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 
use App\Http\Requests\TeacherStoreRequest;
use App\Http\Requests\TeacherUpdateRequest;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::paginate(10);
        $branches = Branch::all();

        return view('pages.dashboard.teachers.overview')
            ->with('teachers', $teachers)
            ->with('branches', $branches);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches = Branch::all();

        return view('pages.dashboard.teachers.create')
            ->with('branches', $branches);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherStoreRequest $request)
    {
        $validated = $request->validated();

        $teacher = Teacher::create($validated);

        // Create an initial status for the teacher.
        $status = Status::create([
            'teacher_id' => $teacher->id,
            'branch_id' => $teacher->branch_id,
            'present' => false
        ]);

        // If we have photos, upload them.
        if($request->has('photos'))
        {
            foreach($request->file('photos') as $photo)
            {
                // We generate a unique filename.
                $name = md5(time() . Str::random(5)) . '.' . $photo->extension();
                $photo->move(public_path(). '\/uploads\/', $name);

                // Associate the photo with the teacher.
                Photo::create([
                    'name' => $name,
                    'teacher_id' => $teacher->id
                ]);
            }
        }

        return redirect()
            ->route('teachers.overview')
            ->with('success', __('zuydpresence.resource_updated'));
    }

    /**
     * Display presence view.
     *
     * @return \Illuminate\Http\Response
     */
    public function presence()
    {
        $statuses = Status::paginate(10);

        return view('pages.dashboard.teachers.presence_view')
            ->with('statuses', $statuses);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        return view('pages.dashboard.teachers.view')
            ->with('teacher', $teacher);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        $departments = Department::where('branch_id', $teacher->branch_id)->get();
        $branches = Branch::all();

        return view('pages.dashboard.teachers.edit')
            ->with('branches', $branches)
            ->with('departments', $departments)
            ->with('teacher', $teacher);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherUpdateRequest $request, Teacher $teacher)
    {
        $validated = $request->validated();

        if(isset($validated['department_id']))
        {
            $department = Department::find($validated['department_id']);

            // Upon branch change, nullify the department.
            if($department->branch_id != $validated['branch_id'])
            {
                $validated['department_id'] = null;
            }
        }

        if(!$request->has('existing_photos'))
        {
            // No existing photos, delete all existing ones.
            Photo::where('teacher_id', $teacher->id)->delete();

            $photos = Photo::where('teacher_id', $teacher->id)->get();

            foreach($photos as $photo)
            {
                File::delete(public_path(). '\/uploads\/' . $photo->name);
            }
        }
        else
        {
            $photos = Photo::where('teacher_id', $teacher->id)->get();

            foreach($photos as $photo)
            {
                // Compare the database and the request and remove photo's
                // that are not present in the request.
                if(!in_array($photo->name, $request->input('existing_photos')))
                {
                    Photo::where('name', $photo->name)->where('teacher_id', $teacher->id)->delete();
                    File::delete(public_path(). '\/uploads\/' . $photo->name);
                }
            }
        }

        // If we new have photos, upload them.
        if($request->has('photos') && $request->file('photos') != null)
        {
            foreach($request->file('photos') as $photo)
            {
                // We generate a unique filename.
                $name = md5(time() . Str::random(5)) . '.' . $photo->extension();
                $photo->move(public_path(). '\/uploads\/', $name);

                // Associate the photo with the teacher.
                Photo::create([
                    'name' => $name,
                    'teacher_id' => $teacher->id
                ]);
            }
        }

        // Update the teacher as normal.
        $teacher->update($validated);

        return redirect()
            ->route('teachers.overview')
            ->with('success', __('zuydpresence.resource_updated'));
    }

    /**
     * Display the teachers for a branch.
     * 
     * @return \Illuminate\Http\Response
     */
    public function focus(Request $request, $branch)
    {
        // Check if we have a valid filter.
        if(is_null($request->route('branch')) || !Branch::where('id', $request->route('branch'))->exists())
        {
            abort(404);
        }
        
        $teachers = Teacher::where('branch_id', $branch)->paginate(10);
        $branches = Branch::all();

        return view('pages.dashboard.teachers.overview')
            ->with('teachers', $teachers)
            ->with('branches', $branches);
    }

    /**
     * Filter the teachers by branch.
     * 
     * @return Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        // If the user requested the 'all' view / filter.
        if($request->input('filter') == 'all')
        {
            return redirect()
                ->route('teachers.overview');
        }

        // Check if we have a valid filter.
        if(!$request->has('filter') || !Branch::where('id', $request->input('filter'))->exists())
        {
            abort(404);
        }

        return redirect()
            ->route('teachers.focus', ['branch' => $request->input('filter')]);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = Teacher::find($id);

        // Delete status object.
        if(Status::where('teacher_id', $id)->count() > 0)
        {
            Status::where('teacher_id', $id)->delete();
        }

        // Delete photos.
        if(Photo::where('teacher_id', $id)->count() > 0)
        {
            $photos = Photo::where('teacher_id', $id)->get();

            foreach($photos as $photo)
            {
                unlink(public_path('uploads/' . $photo->name));
            }
        }

        Photo::where('teacher_id', $id)->delete();

        $teacher->delete();

        return redirect()
            ->route('teachers.overview')
            ->with('success', __('zuydpresence.resource_updated'));
    }
}
