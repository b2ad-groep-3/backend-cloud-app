<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Country;
use App\Models\Teacher;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Requests\BranchStoreRequest;
use App\Http\Requests\BranchUpdateRequest;

class BranchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::paginate(10);

        return view('pages.dashboard.branches.overview')
            ->with('branches', $branches);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();

        return view('pages.dashboard.branches.create')
            ->with('countries', $countries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchStoreRequest $request)
    {
        $validated = $request->validated();

        $branch = Branch::create($validated);

        return redirect()
            ->route('branches.overview')
            ->with('success', __('zuydpresence.resource_updated'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        return view('pages.dashboard.branches.view')
            ->with('branch', $branch);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch)
    {
        $countries = Country::all();

        return view('pages.dashboard.branches.edit')
            ->with('branch', $branch)
            ->with('countries', $countries);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BranchUpdateRequest $request, Branch $branch)
    {
        $updated = $request->validated();

        // If we have departments, delete all of them
        // and create the ones within the request.
        if($request->has('departments'))
        {
            // Get the list of departments currently in the database and request.
            // We flip the array and check for the presence of the key because it is faster.
            $department_database = Department::where('branch_id', $branch->id)->pluck('name', 'id')->toArray();

            // Remove empty array values.
            $request_departments = array_filter($request->input('departments'), function ($var) {
                return $var !== null;
            });

            $request_departments = array_flip($request_departments);

            // If the database has a department that is not in the request, remove it.
            foreach($department_database as $db_id => $db_name)
            {
                // If it does not exists in the database, delete it.
                // Also set the teachers that had this department to a null department.
                if(!isset($request_departments[$db_name]))
                {
                    // Update the teachers that belonged to this department. Nullify the department.
                    Teacher::where('branch_id', $branch->id)->where('department_id', $db_id)->update(['department_id' => null]);

                    // Remove the department.
                    Department::where('branch_id', $branch->id)->where('name', $db_name)->delete();
                }
            }

            // If the request has a department that is not in the database, create it.
            if(count($request_departments) > 0) {
                foreach($request->input('departments') as $req_department)
                {
                    if(!Department::where('branch_id', $branch->id)->where('name', $req_department)->exists())
                    {
                        Department::create([
                            'name' => $req_department,
                            'branch_id' => $branch->id
                        ]);
                    }
                }
            }
        }

        $branch->update($updated);

        return redirect()
            ->route('branches.overview')
            ->with('success', __('zuydpresence.resource_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branch::find($id);

        if(Teacher::where('branch_id', $id)->count() > 0)
        {
            return redirect()
                ->route('branches.overview')
                ->with('fail', __('zuydpresence.branch_has_teachers'));
        }

        $branch->delete();

        return redirect()
            ->route('branches.overview')
            ->with('success', __('zuydpresence.resource_updated'));
    }
}
