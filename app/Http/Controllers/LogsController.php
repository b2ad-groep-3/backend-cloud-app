<?php

namespace App\Http\Controllers;

use App\Models\LogEntry;
use Illuminate\Http\Request;

class LogsController extends Controller
{
    /**
     * Display all logs.
     * 
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $logs = LogEntry::orderBy('created_at', 'desc')->paginate(100);

        return view('pages.dashboard.logs.logs')
            ->with('logs', $logs);
    }

    /**
     * Display the logs for a log group.
     * 
     * @return \Illuminate\Http\Response
     */
    public function focus(Request $request, $group)
    {
        // Requested log group has to exist.
        if(!in_array($group, ['api', 'system', 'other']))
        {
            abort(404);
        }

        $logs = LogEntry::where('group', $group)->orderBy('created_at', 'desc')->paginate(100);

        return view('pages.dashboard.logs.logs')
            ->with('logs', $logs);
    }

    /**
     * Filter the logs by a group.
     * 
     * @return Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        // Check if we have a valid filter.
        if(!$request->has('filter') || !in_array($request->filter, ['all', 'api', 'system', 'other']))
        {
            abort(404);
        }

        if($request->input('filter') == 'all')
        {
            return redirect()
                ->route('logs.all');
        }

        return redirect()
            ->route('logs.focus', ['group' => $request->input('filter')]);
    }

    /**
     * Clear the logs.
     * 
     * @return Illuminate\Http\Response
     */
    public function clear()
    {
        LogEntry::truncate();

        return redirect()
            ->route('logs.all');
    }
}
