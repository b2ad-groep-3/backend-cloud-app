<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class SettingsController extends Controller
{
    /**
     * View the current settings.
     * 
     * @return Illuminate\Http\Response
     */
    public function view()
    {
        return view('pages.settings.settings');
    }

    /**
     * Update the settings.
     * 
     * @return Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(!$request->has('locale') || !in_array($request->input('locale'), array_values(config('zuydpresence.supported_locales'))))
        {
            abort(500);
        }

        Cookie::queue('lang', $request->input('locale'), 262000000);

        if(User::where('id', '!=', auth()->user()->id)->where('email', $request->input('email'))->exists())
        {
            return redirect()
                ->route('settings.view')
                ->with('fail', trans('validation.unique', ['attribute' => 'email']));
        }

        User::where('id', auth()->user()->id)->update(['name' => $request->input('name')]);

        return redirect()
            ->route('settings.view')
            ->with('success', __('zuydpresence.resource_updated'));
    }
}
