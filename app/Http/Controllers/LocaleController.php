<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class LocaleController extends Controller
{
    /**
     * Set the locale in a cookie.
     * 
     * @return Illuminate\Http\Response
     */
    public function setLocaleThroughCookie(Request $request)
    {
        if(!$request->has('locale') || !in_array($request->input('locale'), array_values(config('zuydpresence.supported_locales'))))
        {
            abort(500);
        }

        Cookie::queue('lang', $request->input('locale'), 262000000);

        return redirect()->back();
    }
}
