<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class BranchUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $branch = $this->route('branch');

        return [
            'name' => ['required', 'min:3', 'max:200', 'string', 'regex:/^[a-zA-Z0-9\s]+$/', Rule::unique('branches')->ignore($branch)],
            'street' => ['required', 'min:3', 'max:200', 'string'],
            'house_number' => ['required', 'min:1', 'max:200', 'string', 'alpha_num'],
            'city' => ['required', 'min:3', 'max:200', 'string'],
            'postal_code' => ['required', 'min:3', 'max:200', 'string'],
            'province_state_county' => ['required', 'min:3', 'max:200', 'string'],
            'country_code' => ['required', 'min:1', 'max:2', 'string'],
            'departments' => ['nullable', 'array'],
            'departments.*' => ['nullable', 'string', 'distinct', 'min:3']
        ];
    }
}
