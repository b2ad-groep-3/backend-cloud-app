<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class TeacherUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $teacher = $this->route('teacher');

        return [
            'first_name' => ['required', 'string', 'min:2', 'max:200', 'alpha_num'],
            'middle_name' => ['nullable', 'string', 'min:2', 'max:200', 'alpha_num'],
            'last_name' => ['required', 'string', 'min:2', 'max:200', 'alpha_num'],
            'email' => ['required', 'email', 'min:2', 'max:200', Rule::unique('teachers')->ignore($teacher)],
            'branch_id' => ['required', 'integer'],
            'department_id' => ['nullable', 'integer'],
            'photos' => ['nullable'],
            'photos.*' => ['nullable', 'image', 'max:10240'],
            'existing_photos' => ['nullable'],
            'existing_photos.*' => ['nullable', 'string', 'max:300']
        ];
    }
}
