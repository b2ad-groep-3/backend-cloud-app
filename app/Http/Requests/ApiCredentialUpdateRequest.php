<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ApiCredentialUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $credential = $this->route('api_credential');

        return [
            'reference' => ['required', 'min:3', 'max:200', 'string', 'regex:/^[a-zA-Z0-9\s]+$/', Rule::unique('api_credentials')->ignore($credential)],
            'activated' => ['required', 'string', Rule::in(['active', 'inactive'])],
            'domain' => ['nullable', 'string', 'min:5', 'max:200']
        ];
    }
}
