<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\LogEntry;
use Illuminate\Http\Request;
use App\Models\ApiCredential;
use App\Helpers\ZuydPresenceLog;

class RequiresHMACSignature
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // The previous middleware verified the api key and the request body.
        $header = explode(":", $request->header("X-ZuydPresence-Authorization"));

        $header_method = $header[0];    // HTTP-METHOD
        $header_apikey = $header[1];    // API-KEY
        $header_signature = $header[2]; // SIGNATURE
        $header_epoch = $header[3];     // EPOCH TIMESTAMP

        $body = $request->getContent();

        $secret = ApiCredential::where("key", $header_apikey)->first(["secret"])["secret"];

        if($body != null)
        {
            // The request has a body, include it in the hmac signature.
            $body_content_hash = md5($body);

            $signature_components = implode(":", [$header_method, $header_apikey, $body_content_hash, $header_epoch]);
            $server_signature = hash_hmac('sha256', $signature_components, $secret);
        }
        else
        {
            // The request does not have a body, exclude it from the hmac signature.
            $signature_components = implode(":", [$header_method, $header_apikey, $header_epoch]);
            $server_signature = hash_hmac('sha256', $signature_components, $secret);
        }

        if($header_signature != $server_signature)
        {
            ZuydPresenceLog::apiException('signature_mismatch', $header_apikey, $header_signature);

            return response()->json([
                "status" => "error",
                "message" => "HMAC-signature does not match."
            ], 401);
        }

        return $next($request);
    }
}
