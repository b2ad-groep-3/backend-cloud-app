<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\LogEntry;
use Illuminate\Http\Request;
use App\Models\ApiCredential;
use App\Helpers\ZuydPresenceLog;

class VerifyZuydPresenceAuthorizationHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $header = explode(":", $request->header("X-ZuydPresence-Authorization"));

        $header_method = $header[0];    // HTTP-METHOD
        $header_apikey = $header[1];    // API-KEY
        $header_signature = $header[2]; // SIGNATURE
        $header_epoch = $header[3];     // EPOCH TIMESTAMP

        // Check if theres an api key with the id the request provided.
        if(!ApiCredential::where('key', $header_apikey)->exists())
        {
            ZuydPresenceLog::apiException('nonexistent_apikey', filter_var($header_apikey, FILTER_SANITIZE_STRING));

            return response()->json([
                "status" => "error",
                "message" => "Supplied api key is not valid."
            ], 401);
        }

        $server_credential = ApiCredential::where('key', $header_apikey)->first(['secret', 'activated']);

        // We have an api key, check if it is active.
        if(!$server_credential->activated)
        {
            ZuydPresenceLog::apiException('inactive_apikey', $header_apikey);

            return response()->json([
                "status" => "error",
                "message" => "Supplied api key is marked as inactive."
            ], 401);
        }

        // Check if request method and header method match.
        if($request->method() != $header_method)
        {
            ZuydPresenceLog::apiException('method_mismatch', $header_apikey);

            return response()->json([
                "status" => "error",
                "message" => "Request method does not match the method supplied within the header."
            ], 401);
        }

        // If the request has a body, check if it is valid json.
        if($request->getContent() != null)
        {
            try 
            {
                $content = $request->getContent();
                json_decode($content, false, 512, JSON_THROW_ON_ERROR);
            }
            catch(\JsonException $e)
            {
                ZuydPresenceLog::apiException('invalid_body', $header_apikey);

                return response()->json([
                    "status" => "error",
                    "message" => "Request body is invalid json."
                ], 401);
            }
        }

        return $next($request);
    }
}
