<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\LogEntry;
use Illuminate\Http\Request;
use App\Helpers\ZuydPresenceLog;

class RequiresZuydPresenceAuthorizationHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Check if the header is in the request.
        if(!$request->headers->has("X-ZuydPresence-Authorization"))
        {
            ZuydPresenceLog::apiException('missing_header');

            return response()->json([
                "status" => "error",
                "message" => "Authorization header is missing."
            ], 401);
        }

        // Check if the header has the proper parts within it.
        if(count(explode(":", $request->header("X-ZuydPresence-Authorization"))) != 4)
        {
            ZuydPresenceLog::apiException('malformed_header');

            return response()->json([
                "status" => "error",
                "message" => "Authorization header is malformed."
            ], 401);
        }

        return $next($request);
    }
}
